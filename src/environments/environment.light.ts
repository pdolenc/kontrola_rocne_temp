import { ApplicationSettings } from "src/app/settings/appSettings";

export const environment = {
  production: false,
  USE_EXAMPLE_JWTTOKENS: true,
  ApplicationSettings: {
    DataServiceSettings: {
      BaseUrl : "http://localhost:3000",
      Endpoints: {
        getTezkeKovineFromRemoteDB: "podatki-kal"
      }
    }
  } as ApplicationSettings

};
