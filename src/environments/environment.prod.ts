// kako deployati razlicne enviromente
//https://medium.com/yavar/building-an-angular-application-in-various-environments-using-angular-cli-and-server-18f94067154b

import { ApplicationSettings } from "src/app/settings/appSettings";

export const environment = {
production: false,
USE_EXAMPLE_JWTTOKENS: false,
ApplicationSettings : {
  DataServiceSettings : {
    BaseUrl:"skz-rest-api-lab.apps.msc.arso.sigov.si"
  }
} as ApplicationSettings};
