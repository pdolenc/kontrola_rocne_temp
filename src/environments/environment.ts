// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.

import { ApplicationSettings } from "src/app/settings/appSettings";


export const environment = {
  production: false,
  AUTH_URL:"localhost:80",
  USE_EXAMPLE_JWTTOKENS: false,
  ApplicationSettings : {
    DataServiceSettings : {
      BaseUrl:"http://localhost:8001",
    }
  } as ApplicationSettings
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
