import { Component, Input, OnInit } from '@angular/core';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { IInputInfoValue, InputInfo, InputInfoController } from '../helperClasses/input-info-classes';

@Component({
  selector: 'app-input-dropdown',
  templateUrl: './input-dropdown.component.html',
  styleUrls: ['./input-dropdown.component.css']
})
export class InputDropdownComponent implements OnInit {

  @Input()
  InputData! : InputInfoController;


  SelectValue(val : IInputInfoValue)
  {
    if(this.InputData.inputInfoSubject.getValue().getClickable())
      this.InputData.inputInfoSubject.getValue().selectValue(val);
    else
      console.log("unclickable value");
  }
  constructor() {   }

  ngOnInit(): void {
    this.InputData.inputInfoSubject.subscribe((newInputInfo) =>{this.InputData = newInputInfo; console.log("new II" + newInputInfo.name);});
  }

}
