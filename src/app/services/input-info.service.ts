import { Injectable, Input } from '@angular/core';
import { Type } from "@angular/core";
import { map, Observable } from 'rxjs';
import { IInputInfoValue, InputInfo, ObservingInputInfo, SimpleStringInputInfo } from '../helperClasses/input-info-classes';
import { InputDropdownComponent } from "../input-dropdown/input-dropdown.component";
import { DataService } from './data.service';
import { DataRepositoryService } from './data-repository.service';
import { ValueInputInfo } from '../helperClasses/input-info-classes';


/**
 * Service je namenjen pridobivanju podatkov, ki jih potrebujejo InputInfoti
 * (to so v praksi oz v UI dropdown-i, v katerih izbiramo parametre za prikaz podatkov)
 */
@Injectable({
  providedIn: 'root'
})
export class InputInfoService {

  constructor(private dataRepo: DataRepositoryService, private dataService: DataService) { }

  //#region statični predefined objekti
  public readonly MESEC: InputInfo = new SimpleStringInputInfo("Mesec", [
    "januar",
    "februar",
    "marec",
    "april",
    "maj",
    "junij",
    "julij",
    "avgust",
    "september",
    "oktober",
    "november",
    "december"
  ], "MESEC");
  //veačanje indeksa izbranega meseca
  public GetMonthNumber(monthString: string): number | undefined {
    const months: Record<string, number> = {
      januar: 1,
      februar: 2,
      marec: 3,
      april: 4,
      maj: 5,
      junij: 6,
      julij: 7,
      avgust: 8,
      september: 9,
      oktober: 10,
      november: 11,
      december: 12,
    };
    return months[monthString.toLowerCase()];
  }

  //pridobivanje zadnjih 15 let:
  getYears = (): string[] => {
    const currentYear = new Date().getFullYear();
    return Array.from({ length: 16 }, (_, index) => (currentYear - index).toString()).reverse();
  };
  public readonly LETO: InputInfo = new SimpleStringInputInfo("Leto", this.getYears(), 'LETO')



  //#endregion

  //#region statične metode
  //Za kreiranje InputInfo po meri
  public CustomInputInfo(name: string, values: String[]) {
    return new SimpleStringInputInfo(name, values);
  }

  //Za kreiranje InputInfo, ki temelji na parametrih
  //Če jih ni v LocalStorage, jih pridobimo iz backenda 
  public GetParametriInputInfo(name: string, ID: string, parameter: string, onReceivedInputInfoValuesCallback: ((data: IInputInfoValue[]) => void) | null = null): InputInfo {
    let data = this.dataRepo.GetParameter(parameter);
    if (data instanceof Observable)
      return new ObservingInputInfo(name, data.pipe(map(allData => allData.map<IInputInfoValue>((data) => (({ value: data.parameterIme, id: data.parameterID }) as IInputInfoValue)))), ID, onReceivedInputInfoValuesCallback);
    else {
      var II = new ValueInputInfo(name, data.map((data) => (({ value: data.parameterIme, id: data.parameterID }) as IInputInfoValue)), ID);
      if (onReceivedInputInfoValuesCallback != null)
        onReceivedInputInfoValuesCallback(II.getValues());
      return II;
    }
  }


  //pridobivanje postaj. 
  public GetPostajeInputInfo(name: string, tip: string, datum_od: Date, datum_do: Date, trajanje: string, ID: string | null): InputInfo {
    let data = this.dataRepo.GetPostaje(tip, datum_od, datum_do, trajanje);
    if (data instanceof Observable)
      return new ObservingInputInfo(name, data.pipe(map(allData => allData.map<IInputInfoValue>((data) => (({ value: data.postajaIme, id: data.id }) as IInputInfoValue)))), ID);
    else
      return new ValueInputInfo(name, data.map((data) => (({ value: data.postajaIme, id: data.id }) as IInputInfoValue)), ID);
  }
  //#endregion

}


