import { Inject, Injectable } from '@angular/core';
import { firstValueFrom, Observable, Observer, Subject, timestamp } from 'rxjs';
import { IInputInfoValue } from '../helperClasses/input-info-classes';
import { LOCAL_STORAGE } from '../helperClasses/local-storage-classes';
import { KljuciResponse } from '../models/Responses/KljuciResponse';
import { PostajaBase } from '../models/Responses/MeritveResponse';
import { ParametriResponse } from '../models/Responses/ParametriResponse';
import { AdditionalEmepInfo, TableInputDataType } from '../table-host/table-host.component';
import { DataService, EMEPData } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class DataRepositoryService {

  //private static readonly cache: Storage = localStorage;
  //čas shranjevanja podatkov (v urah)
  private readonly CACHE_LIFE_IN_HOURS: number = 8;
  private readonly KEY_PREFIX: string = "ARSO_DATA_";

  constructor(private dataService: DataService, @Inject(LOCAL_STORAGE) private cache: Storage) { }

  //#region working with local storage
  //izpraznimo cache
  public ClearCache(){
    this.cache.clear();
  }

  //pridobi podatke, shranjene v lokalni shrambi brskalnika
  private getItem(key: String): CacheData | null {
    const _key = this.generateKey(key);
    let data = this.cache.getItem(_key as string);
    if (data != null)
      return JSON.parse(data) as CacheData;
    else
      return null;
  }
  //pridobi SAMO VELJAVNE podatke iz lokalne baze
  private getValidItem(key: string): CacheData | null {
    var item = this.getItem(key);
    if (item != null && this.validateData(item))
      return item;
    else
      return null;
  }
  //nastavi podatke za lokalno shrambo v brskalniku
  private setItem(key: String, data: any) {
    const _key = this.generateKey(key);
    const jsonData = JSON.stringify(new CacheData(data, Date.now()));
    this.cache.setItem(_key as string, jsonData);
  }


  //generira ključ za lokalno shrambo, da se podatki slučajno ne pomešajo z ostalimi
  private generateKey(key: String): string {
    return this.KEY_PREFIX + key;
  }
  //preveri, če so podatki v shrambi še veljavni
  private validateData(data: CacheData): boolean {
    let expDate = new Date(data.timestamp + (1000 * 60 * 60 * this.CACHE_LIFE_IN_HOURS));
    return expDate > new Date();
  }
  //#endregion

  //pridobivanje parametra. Če parametra ni v shrambi, ga pridobimo iz backenda
  public GetParameter(tip: string): Observable<ParametriResponse[]> | ParametriResponse[] {
    let localData = this.getValidItem(tip);
    if (localData == null) {
      console.log("No valid data found for key " + tip);
      let httpData = this.dataService.getParameters(tip);
      httpData.subscribe({
        next: (data) => this.setItem(tip, data),
        error: () => console.log("Error saving data for key " + tip),
        complete: () => console.log("Data for key " + tip + " saved in local storage ")
      });
      return httpData;
    } else {
      console.log("Valid data found for key " + tip);
      return localData.data;
    }
  }

  //pridobivanje EMEP zastavic parametra. Če jih ni v lokalni shrambi, jih pridobimo iz backenda
  //EMEP zastavice imajo trimestno stevilko in vrednost. Kjer je stevilka null, tam ni EMEP zastavice
  public async GetEmepData(tip: string, additionalEmepInfo: AdditionalEmepInfo[], includeEmpty: boolean = true): Promise<EMEPData[]> {
    //pridobimo podatke
    var emepData;
    let localData = this.getValidItem(`EMEP_${tip}`);
    if (localData == null) {
      emepData = await firstValueFrom(this.dataService.getEmepData(tip));
      //nastavimo se posebnosti (forsiranje stanja ipd)
      emepData = this.addAdditionalEmepData(emepData, additionalEmepInfo);
      this.setItem(`EMEP_${tip}`, emepData);
    } else {
      emepData = (localData.data as EMEPData[]).map<EMEPData>(emepvalues => new EMEPData(emepvalues.Text, emepvalues.ForceInvalid, emepvalues.ID, emepvalues.ForceValid,emepvalues.ForcePower, emepvalues.ForceStanje));
    }

    if (includeEmpty) { return [EMEPData.Empty, ...emepData] }
    else return emepData;

  }

  //Pridobivanje definicije strukture dolocene padavine
  public async GetKljuci(tip: string): Promise<KljuciResponse> {
    let localData = this.getValidItem(`KLJUCI_${tip}`);
    if (localData == null) {
      console.log(`Kljuci for tip ${tip} not in cache`);
      var data = await firstValueFrom(this.dataService.getKljuci(tip));
      this.setItem(`KLJUCI_${tip}`, data);
      console.log(`Kljuci for tip ${tip} cached`);
      return data;
    } else {
      return localData.data as unknown as KljuciResponse;
    }
  }



  //pridobimo postaje za določeno obdobje
  public GetPostaje(tip: string, datum_od: Date, datum_do: Date, trajanje: string): Observable<PostajaBase[]> | PostajaBase[] {
    let localData = this.getValidItem(`POSTAJE_${tip}_${datum_od.getTime()}_${datum_do.getTime()}_${trajanje}`);
    if (localData == null) {
      //pridobimo observable iz service-a
      var data = this.dataService.getPostaje(tip, datum_od, datum_do, trajanje);
      data.subscribe({
        next: (data) => {
          this.setItem(`POSTAJE_${tip}_${datum_od.getTime()}_${datum_do.getTime()}_${trajanje}`, data);
        }
      })
      //subscribamo na observable - pridobljene podatke zacachiramo
      return data;
    } else {
      return localData.data as unknown as PostajaBase[];
    }
  }

  //zastavicam nastavimo se dodaten info
  private addAdditionalEmepData(emepData: EMEPData[], additionalEmepInfo: AdditionalEmepInfo[]): EMEPData[] {
    additionalEmepInfo.forEach(addData => {
      var emepToChange = emepData.find(emep => emep.ID == addData.ID);
      if (emepToChange == null) {
        console.error(`Definirana je posebna zastavica za ${addData.ID}, ki pa je nismo pridobili iz backenda! `);
        return;
      } else {
        emepToChange.ForceStanje = addData.ForceStanje;
      }
    });
    return emepData;
  }




}

//definicija podatkov, ki jih shranjujemo v lokalno shrambo
class CacheData {
  constructor(public data: any, public timestamp: number) { }
}

