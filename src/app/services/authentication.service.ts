import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { catchError, Observable, retry, tap, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { LOCAL_STORAGE } from '../helperClasses/local-storage-classes';
import { JwtTokenWrapper, User } from '../helperClasses/authentication-classes';

import * as ExampleValidRefreshToken from '../jsonData/ExampleValidRefreshToken.json';
import * as ExampleExpiredRefreshToken from '../jsonData/ExampleExpiredRefreshToken.json';
import { ApplicationSettings } from '../settings/appSettings';
import { AppSettings } from '../app.module';
import { DataServiceSettingsHandler } from './data.service';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private _settings : DataServiceSettingsHandler;
  constructor(private http: HttpClient, @Inject(LOCAL_STORAGE) private localStorage: Storage,@Inject(AppSettings) appsettings: ApplicationSettings) {

      this._settings = new DataServiceSettingsHandler(appsettings.DataServiceSettings!);
  }

  //poskusimo se prijaviti z uporabnipkim imenom in geslom
  public logIn(username: string, email:string, password: string) {
    if(environment.USE_EXAMPLE_JWTTOKENS)
    {
      console.log("adding example json token");
      this.jwtTokenSetter(ExampleValidRefreshToken);
      return;
    }
    console.log("environment.USE_EXAMPLE_JWTTOKENS");

    let body = new HttpParams()
      .set("email", email)
      .set("password", password);
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')

    return this.http.post<JwtTokenWrapper>(this._settings.GetEndpoint("logIn"), body, { headers: headers })
      .pipe(
        tap((result: JwtTokenWrapper) => {
          this.jwtTokenSetter(result);
        })
      );

  }

  //depricated
  public register(username: string, email:string,password: string): Observable<JwtTokenWrapper> {
    let body = new HttpParams()
      .set("username", username)
      .set("password", password)
      .set("email", email);

    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')

    return this.http.post<JwtTokenWrapper>(this._settings.GetEndpoint("register"), body, { headers: headers })
      .pipe(
        tap((result: JwtTokenWrapper) => {
          this.jwtTokenSetter(result);
        })
      );
  }

  //odjava - iz shrambe odstranimo JWT zeton
  public logOut(): void {
    console.log("removing token");
    this.localStorage.removeItem('ARSO_JWT_token');
  }
  //pridobimo JWT zeton, ki ga uporabljamo za identifikacijo in avtorizacijo pri klicih na backend
  private jwtTokenGetter(): JwtTokenWrapper | null {
    console.log("getting token");
    var token = this.localStorage.getItem("ARSO_JWT_token");
    if(token == null)
      return null;
    return JSON.parse(token);
  }
  //nastavimo JWT zeton
  private jwtTokenSetter(token: JwtTokenWrapper) {
    console.log("setting token");
    this.localStorage.setItem("ARSO_JWT_token", JSON.stringify(token));
  }

  //preverjamo, ali je uporabnik trenutno prijavljen (iz shrambe brskalnika pridobimo zeton in preverimo njegovo veljavnost)
  public isUserLoggedIn(): boolean {
    const tokens: JwtTokenWrapper | null = this.jwtTokenGetter();
    if (tokens) {
        //TODO prilagodi na trenutno strukturo JWT zetona
      const koristnaVsebina = JSON.parse(atob(tokens.refresh.split('.')[1]));
      //trenutno se smatra, da je exp das, ko token poteče
      return koristnaVsebina.exp > Date.now() ;
    } else {
      return false;
    }
  }

  public loggedUserData(): User | null {
    if (this.isUserLoggedIn()) {
      const zeton: JwtTokenWrapper= this.jwtTokenGetter()!;
      if (zeton) {
          //TODO prilagodi na trenutno strukturo JWT zetona
       var userData = JSON.parse(
          atob(zeton.refresh.split('.')[1])
        );
        console.log(userData);
        return userData as User;
      }
    }
    return null;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('Auth service: Client side error:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Auth service: Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new Error('Avtentikacija: Pri pridobivanju podatkov je prišlo do napake!'));
  }
}
