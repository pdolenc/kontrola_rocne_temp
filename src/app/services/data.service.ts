import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { catchError, first, map, Observable, pipe, retry, tap, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { IInputInfoValue } from '../helperClasses/input-info-classes';
import { InsertMeritev, InsertMeritevRequest } from '../models/Requests/InsertTezkeKovineRequest';
import { UpdateMeritev, UpdateMeritveRequest } from '../models/Requests/UpdateMeritveRequest';
import { KljuciResponse } from '../models/Responses/KljuciResponse';
import { MeritevResponseBase, Postaja, PostajaBase } from '../models/Responses/MeritveResponse';
import { ParametriResponse } from '../models/Responses/ParametriResponse';
import { PMDelciMeritev, PMDelciResponse } from '../models/Responses/PMDelciResponse';
import { ITedniVLetuResponse, TedenData } from '../models/Responses/TedniVLetuResponse';
import { TezkeKovineMeritev, TezkeKovineResponse } from '../models/Responses/TezkeKovineResponse';
import { CellDataWrapper, Stanje } from '../table-host/tableClasses';
import { DataRepositoryService } from './data-repository.service';
import { PadavineResponseBase } from "../models/Responses/Padavine";
import { AppSettings } from '../app.module';
import { ApplicationSettings, DataServiceSettings } from '../settings/appSettings';

const httpPostOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

export class DataServiceSettingsHandler {

  constructor(public allSettings: DataServiceSettings) {
    console.log({ AppSettings: allSettings });
  }

  public GetEndpoint(endpointID: string) {
    console.log(this.allSettings);
    return this.generateEndpoint(this.allSettings!.Endpoints![endpointID]);
  }
  //generiranje url-ja iz končnice
  private generateEndpoint(endpoint: String) {
    return `${this.allSettings.BaseUrl}/${endpoint}`;
  }
}

/**
 * Service je namenjen vsem klicem na backend
 * in transformaciji podatkov iz backenda za nadaljno uporabo
 */
@Injectable({
  providedIn: 'root'
})
export class DataService {

  private _settings: DataServiceSettingsHandler;
  constructor(private http: HttpClient, @Inject(AppSettings) appsettings: ApplicationSettings) {

    this._settings = new DataServiceSettingsHandler(appsettings.DataServiceSettings!);
  }





  /**
   * Pridobivanje padavin
   * @date 8/27/2023 - 8:16:26 PM
   *
   * @public
   * @param {string} tip
   * @param {string} postajaID
   * @param {number} trajanje
   * @param {string} datumOd
   * @param {string} datumDo
   * @returns {PadavineResponseBase[]}
   */
  public GetPadavine(tip: string, postajaID: string, trajanje: number, datumOd: string, datumDo: string) {
    let requestOptions: Object = {
      params: new HttpParams()
        .set('tip', tip)
        .set('datum_od', datumOd)
        .set('datum_do', datumDo)
        .set('postaja', postajaID)
        .set('trajanje', trajanje)
    }
    return this.http.get<PadavineResponseBase[]>(this._settings.GetEndpoint("GetPadavine"), requestOptions)
      .pipe(
        retry(2), // retry a failed request up to 2 times
        catchError(this.handleError) // then handle the error
      );
  }



  //pridobivanje parametrov glede na tip iz backenda
  public getParameters(tip: String): Observable<ParametriResponse[]> {
    let requestOptions: Object = {
      params: new HttpParams()
        .set('tip', tip as string)
    }
    return this.http.get<any[]>(this._settings.GetEndpoint("getParameters"), requestOptions)
      .pipe(
        retry(2), // retry a failed request up to 2 times
        catchError(this.handleError) // then handle the error
      );

  }

  //pridobivanje postaj iz backenda
  public getPostaje(tipMeritve: string, odDat: Date, doDat: Date, trajanje: string = "+01 00:00:00"): Observable<PostajaBase[]> {
    //console.log("Getting postaje from backend");
    console.log(`${tipMeritve}, ${odDat}, ${doDat}`);
    let requestOptions: Object = {
      params: new HttpParams()
        .set('tip', tipMeritve)
        .set('datum_od', odDat.toJSON())
        .set('datum_do', doDat.toJSON())
        .set('trajanje', trajanje)
    }
    return this.http.get<PostajaBase[]>(this._settings.GetEndpoint("getPostaje"), requestOptions)
      .pipe(
        retry(2), // retry a failed request up to 2 times
        catchError(this.handleError) // then handle the error
      );
  }

  //pridobivanje parametrov iz backenda
  public getParametri(tip: string) {
    console.log("Getting parametri from backend");
    let requestOptions: Object = {
      params: new HttpParams()
        .set('tip', tip)
    }
    return this.http.get<any[]>(this._settings.GetEndpoint("getParametri"), requestOptions)
      .pipe(
        retry(2), // retry a failed request up to 2 times
        catchError(this.handleError) // then handle the error
      );
  }

  //pridobivanje vseh tednov v dolocenem letu
  public getTedniVLetu(leto: number) {
    //http://127.0.0.1:8001/tedni/?leto=2022
    console.log(`Getting tedni v letu ${leto} from backend`);

    let requestOptions: Object = {
      params: new HttpParams()
        .set('leto', leto)
    }
    return this.http.get<TedenData[]>(this._settings.GetEndpoint("getTedniVLetu"), requestOptions)
      .pipe(
        map(tedni => ({ tedni: tedni } as ITedniVLetuResponse)),
        retry(2), // retry a failed request up to 2 times
        catchError(this.handleError) // then handle the error
      );
  }

  //pridobivanje tezkih kovin iz backenda
  public getPMDelci(httpParams: HttpParams): Observable<PMDelciResponse> {
    console.log("Getting pm delci from backend");
    let requestOptions: Object = {
      params: httpParams
    }
   // console.log(JSON.stringify(requestOptions));
    //console.log("DEBUG URL: " + this.DEBUG_DisplayCall("getPMDelci", httpParams));

    return this.http.get<Postaja<PMDelciMeritev>[]>(this._settings.GetEndpoint("getPMDelci"), requestOptions)
      .pipe(
        map(postaje => { return new PMDelciResponse(postaje) }),
        retry(2), // retry a failed request up to 2 times
        catchError(this.handleError) // then handle the error
      );
  }
  //pridobivanje tezkih kovin iz remote backenda
  public getPMDelciFromRemoteDB(httpParams: HttpParams): Observable<PMDelciResponse> {
    console.log("Getting pm delci from remote DB backend");
    let requestOptions: Object = {
      params: httpParams
    }
    //console.log("DEBUG URL: " + this.DEBUG_DisplayCall("podatki-pm-kal", httpParams));
    return this.http.get<Postaja<PMDelciMeritev>[]>(this._settings.GetEndpoint("getPMDelciFromRemoteDB"), requestOptions)
      .pipe(
        map(postaje => { console.log(postaje); return new PMDelciResponse(postaje) }),
        retry(2), // retry a failed request up to 2 times
        catchError(this.handleError) // then handle the error
      );
  }

  private DEBUG_DisplayCall(url: string, params: HttpParams): string {
    let queryString = '';

    // Iterate over the parameters
    params.keys().forEach(key => {
      const value = params.get(key);

      // Encode key and value
      const encodedKey = encodeURIComponent(key);
      const encodedValue = value;
      //const processedValue = encodedValue.replace(/%2C/g, ',');

      // Append the parameter to the query string
      queryString += `${encodedKey}=${encodedValue}&`;
    });

    // Remove the trailing '&' character
    queryString = queryString.slice(0, -1);

    // Return the query string
    return `${this._settings.GetEndpoint(url)}?${queryString}`;
  }

  //pridobivanje tezkih kovin iz backenda
  public getTezkeKovine(httpParams: HttpParams): Observable<TezkeKovineResponse> {
    console.log("Getting tezke kovine from backend");
    let requestOptions: Object = {
      params: httpParams
    }
    //console.log(JSON.stringify(requestOptions));
    return this.http.get<Postaja<TezkeKovineMeritev>[]>(this._settings.GetEndpoint("getTezkeKovine"), requestOptions)
      .pipe(
        map(postaja => postaja != null && postaja.length != 0 ? new TezkeKovineResponse(postaja[0]) : new TezkeKovineResponse(null)),
        retry(2), // retry a failed request up to 2 times
        catchError(this.handleError) // then handle the error
      );
  }

  //pridobivanje tezkih kovin iz backenda
  //! treba je bilo preuirediti podatke, ker jih api na vnos v bazo pričakuje v drugačni obliki, kot pa so sem prišli
  public getTezkeKovineFromRemoteDB(httpParams: HttpParams): Observable<TezkeKovineResponse> {
    console.log("Getting tezke kovine from remote db");
    let requestOptions: Object = {
      params: httpParams
    }
    //console.log(JSON.stringify(requestOptions));
    return this.http.get<Postaja<TezkeKovineMeritev>[]>(this._settings.GetEndpoint("getTezkeKovineFromRemoteDB"), requestOptions)
      .pipe(
        map(postaja => postaja != null && postaja.length != 0 ? new TezkeKovineResponse(postaja[0]) : new TezkeKovineResponse(null)),
        retry(2), // retry a failed request up to 2 times
        catchError(this.handleError) // then handle the error
      );
  }

  //TODO endpoint
  //pridobivanje emep zastavic za dolocen tip padavin backenda
  public getEmepData(tip: string) {
    console.log("Getting emepData backend");
    let requestOptions: Object = {
      params: new HttpParams()
        .set('tip', tip)
    }
    //console.log(JSON.stringify(requestOptions));
    return this.http.get<any[]>(this._settings.GetEndpoint("getEmepData"), requestOptions)
      .pipe(
        //tap(res => console.log(res)),
        map(response => response.map<EMEPData>((emepData => new EMEPData(emepData.opis, emepData.veljavnost == -1 ? true : false, emepData.zastavica, emepData.veljavnost == 1 ? true : false)))),
        retry(2), // retry a failed request up to 2 times
        catchError(this.handleError) // then handle the error
      );
  }


  public UpdateCellsInDB(meritve: UpdateMeritev[]) {
    console.log("updating cells");
    //console.log(JSON.stringify(requestOptions));
    //TODO! preveri in poenoti. To je samo za test.
    var meritveDTO = meritve.map(mer => { var meritevDTO = { ...mer, datum_zajema: this.formatDateToISOString(mer.datum_zajema) }; return meritevDTO; })

    //console.log({ meritveToUpdate: meritve, fixedDate:meritveDTO  });
    console.log({ meritveToUpdate: meritve });
    return this.http.post<MeritevResponseBase[]>(this._settings.GetEndpoint("UpdateCellsInDB"), meritveDTO, httpPostOptions)
      .pipe(
        retry(2), // retry a failed request up to 2 times
        catchError(this.handleError) // then handle the error
      );
  }

  //transformiranje datuma v format, ki ga pričakuje backend
  private formatDateToISOString(date: Date): string {
    if (date == null)
      return date;
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are 0-indexed.
    const day = String(date.getDate()).padStart(2, '0');
    //console.log({y: year, m: month, d: day, date: date});
    return `${year}-${month}-${day}T00:00Z`;
  }



  public insertData(data: InsertMeritev[]) {
    //console.log(JSON.stringify(requestOptions));
    return this.http.post<MeritevResponseBase[]>(this._settings.GetEndpoint("insert_data"), { meritve: data } as InsertMeritevRequest, httpPostOptions)
      .pipe(
        retry(2), // retry a failed request up to 2 times
        catchError(this.handleError) // then handle the error
      );
  }
  public getKljuci(tip: string): Observable<KljuciResponse> {
    let requestOptions: Object = {
      params: new HttpParams()
        .set('tip', tip)
    }
    //vedno dobimo samo en kljuc
    return this.http.get<KljuciResponse[]>(this._settings.GetEndpoint("getKljuci"), requestOptions)
      .pipe(
        map(kljuci => {
          if (kljuci != undefined && kljuci.length > 0)
            return kljuci[0];
          throw `klic na ${this._settings.GetEndpoint("getKljuci")} ni vrnil rezultata`;
        }),
        retry(2),
        catchError(this.handleError)
      )
  }




  //kako obravnavamo errorje, ki pridejo iz backenda
  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('Client side error:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new Error('Pri pridobivanju podatkov je prišlo do napake!'));

  }

}



//podatki emep zastavic.
export class EMEPData {
  //nova logika: sedaj je potreba tudi po zastavicah, ki forsirajo veljavnost (ne samo neveljavnost). Za to je dodan paroperty ForceInvalid.
  //oba (ForceValid in ForceInvalid ) ne smeta bit true. Če sta, ima prednost ForceInvalid
  //prav tako je dodan property ForcePower - kako močno forsiranje je. Če imamo več zastavic - ena forsira validnost, druga pa nevalidnost bo zmagala tista, ki ima večjo moč. Privzeto je 0.
  public ForceStanje: Stanje;
  constructor(public readonly Text: string, public readonly ForceInvalid: boolean, public readonly ID?: number, public readonly ForceValid: boolean = false, public readonly ForcePower: number | null = null, ForceStanje: Stanje = Stanje.Brez) {
    this.ForceStanje = ForceStanje;
    //po defaultu je za zastavico, ki forsira neveljavnost, moč 100
    if (ForceInvalid && ForcePower == null)
      this.ForcePower = 100;
    //za tiste, ki forsirajo veljavnost, pa je moč 200
    else if (ForceValid && ForcePower == null)
      this.ForcePower = 200;
  }
  public get DisplayFull() {
    return `${this.DisplayID}:${this.Text}`
  }
  public get DisplayID() {
    if (this.ID == null)
      return '';
    else
      return `#${this.ID}`

  }
  //glede na veljavnost, ki jo forsira zastavica (ce jo ) vrnemo njeno barvo
  public get DisplayColor() {
    return this.ForceInvalid == true ? "red" : (this.ForceValid == true ? "green" : "whitesmoke");
  }
  //enako kot DisplayColor, samo da v primeru nevralnosti vrača temno barvo
  public get DisplayColorDark() {
    return this.ForceInvalid == true ? "red" : (this.ForceValid == true ? "green" : "black");
  }
  public static Empty = new EMEPData("Brez zastavice", false)
  public static Unknown = new EMEPData("NEZNANA ZASTAVICA! Preverite stanje v bazi", false)
}




