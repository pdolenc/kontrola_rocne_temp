import { Component, ComponentFactoryResolver, Input, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { InputInfo, InputInfoController } from '../helperClasses/input-info-classes';
import { InputDropdownComponent } from '../input-dropdown/input-dropdown.component';
import { IMeritevHandler } from '../glavneKomponente/meritev-handler';

@Component({
  selector: 'app-input-collector',
  templateUrl: './input-collector.component.html',
  styleUrls: ['./input-collector.component.css']
})
export class InputCollectorComponent implements OnInit {
  

  constructor() { }

  @Input() inputs! : InputInfoController[];
  @Input() name: String | null= null;
  @Input() handler! : IMeritevHandler;
  @ViewChildren('inputComponent') inputComponents!: QueryList<InputDropdownComponent>;
  get isInitialized() : boolean {return this.name != null}
  public displayErrorButton : boolean = false;

  private clearButtonTimer?: ReturnType<typeof setTimeout>;
  
  ngOnInit(): void {
    
  }

  ngAfterViewChecked() {
  
  } 
  ngAfterViewInit() {
  
  }
  
  setInputs(inputs: InputInfoController[]) {
    this.inputs = inputs;//inputs.map<InputInfo>((cont)=>cont.inputInfoSubject.getValue())
  }
  GetInputData(){
    let outputData : InputInfo[] = [];
    this.inputComponents.forEach(comp => {outputData.push(comp.InputData.inputInfoSubject.getValue());});

     //ce niso izbrani vsi podatki, potem samo pokazemo error button
     if(!this.AllInputInfoSelected(outputData)){
      this.displayErrorButtonTrigger();
      return;
    }

    this.handler.receiveInputData(outputData,this.name!);
    //this.selectedValue_observer.next(outputData);
  }

  private AllInputInfoSelected(data: InputInfo[]) : boolean{
    var allDataSelected = true;
    data.forEach(
      (inputInfo) => {
        if(!inputInfo.getClickable || inputInfo.selectedValueSubject?.getValue() == null || inputInfo.selectedValueSubject?.getValue() == undefined)
        allDataSelected =  false;
      }
    );
    return allDataSelected;
  }
  async displayErrorButtonTrigger(){
    this.displayErrorButton = true;
    if(this.clearButtonTimer != null)
      clearTimeout(this.clearButtonTimer);
    this.clearButtonTimer =  setTimeout(() =>{this.displayErrorButton = false;},1200);
  }
}
  
