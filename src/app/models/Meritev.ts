//vsi podatki, ki jih potrebujemo, da lahko identificiramo meritev (celico)
export interface Meritev {
    tip_meritve: string;
    datum_zajema: Date;
    instalacija: string;
    trajanje: number;
    parameter: number;
}
