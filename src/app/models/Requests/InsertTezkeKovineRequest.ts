import { Meritev } from "../Meritev";

export interface Podparametri {
    depozicija: number;
    koncentracija: number;
    koncentrac_suh_use: number;
    volumen: number;
    volumen_suh_use: number;
    veljavnost: number;
}

export interface JsonVzorec {
    cas_vzorcenja: number;
    oznaka_vzorca: string;
    lab_stevilka_padavina: number;
}

export interface JsonKontrola {
    lod: number;//neskladje
    loq: number;//neskladje
    tkd_premer_lijaka: number; //neskladje
}

export interface InsertMeritev extends Meritev{
    podparametri: Podparametri;
    emep: number[];//neskladje
    json_vzorec: JsonVzorec;
    json_kontrola: JsonKontrola;
}
export interface InsertMeritevRequest {
    meritve : InsertMeritev[];
}



