import { Meritev } from "../Meritev";

export interface UpdateMeritveRequest{
  meritve : UpdateMeritev[];
}

export interface UpdateMeritev extends Meritev{
    emep: number[];
    veljavnost: number;
    komentar : string;
    //opomba_skz: string;
}
