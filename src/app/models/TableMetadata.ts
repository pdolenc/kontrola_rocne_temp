import { CellPropertyDefinition, CellPropertyDefinitionCollection, ColumnDataWrapper } from "../table-host/tableClasses";

export interface AdditionalColumn<T> {
    ColumnDataWrapper: ColumnDataWrapper;
    ColumnInfo: T,
    CellPropertyDefinition : CellPropertyDefinitionCollection
}
export interface TableMetadataBase{

}
export interface AdditionalColumnsTableMetaData<T> extends TableMetadataBase{
    additionalColumns : AdditionalColumn<T>[];
}