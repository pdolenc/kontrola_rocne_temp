export interface ITedniVLetuResponse {
    tedni: TedenData[];
}

export interface TedenData {
    leto: string;
    tedenStevilka: string;
    datumOd: Date;
    datumDo: Date;
}