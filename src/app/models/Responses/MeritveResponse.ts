import { Meritev } from "../Meritev";

export interface MeritevResponseBase extends Meritev {
    success: boolean;
    error: string;
}


export interface Podatki<T extends Meritev> {
    parameterID: number;
    parameterIme: string;
    meritve: T[];
}

export interface Postaja<T extends Meritev> extends PostajaBase {
    podatki: Podatki<T>[];
}

export interface PostajaBase{
    id: string;
    postajaIme: string;
}