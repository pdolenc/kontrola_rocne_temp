import { TableInputDataType } from "src/app/table-host/table-host.component";
import { Meritev } from "../Meritev";
import { Podatki, Postaja } from "./MeritveResponse";
import { ResponseBase } from "./ResponseBase";

export interface TezkeKovineMeritev extends Meritev {
    termin: Date;
    koncentracija: number;
    koncentrac_suh_use: number;
    depozicija: number;
    veljavnost_skz: number;
    masa: string;
    za_objavo?: boolean;
    emep: string;
    volumen: number;
    volumen_suh_use: number;
    veljavnost_kal: number; // ali je ze v novi bazi
    tkd_premer_lijaka: string; // zakaj je to string?
    lod: string;               // zakaj je to string?
    loq: string;               // zakaj je to string?
    opomba_lab: string;
    opomba_merilno_mesto: string;
    komentar: string; //opomba, ki jo lahko dodamo ob preverjanju podatkov. Velja za celo vrstico
}


export class TezkeKovineResponse extends ResponseBase implements Postaja<TezkeKovineMeritev> {
    id!: string;
    postajaIme!: string;
    podatki!: Podatki<TezkeKovineMeritev>[];
    constructor(interfaceData: Postaja<TezkeKovineMeritev> | null) {
        super();
        if (interfaceData == null)
            return;
        this.id = interfaceData.id;
        this.postajaIme = interfaceData.postajaIme
        this.podatki = interfaceData.podatki;
        this.podatki.forEach(podatek => {
            podatek.meritve.forEach(meritev => {
                meritev.tip_meritve = TableInputDataType.TezkeKovine,
                    meritev.instalacija = this.id,
                    meritev.datum_zajema = new Date(meritev.termin)
                meritev.parameter = podatek.parameterID
            });
        });
    }
}



