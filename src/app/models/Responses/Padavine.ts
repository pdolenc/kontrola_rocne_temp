
export interface PadavineResponseBase {
    postajaID: string;
    postajaIme: string;
    trajanje_meritve: number;
    podatek_opis: string;
    tip_meritve: string;
    enota: string;
    vrednost: number;
    veljavnost: number;
    termin: Date;
    datumDo: string;
}