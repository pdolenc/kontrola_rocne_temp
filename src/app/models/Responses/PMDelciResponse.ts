import { TableInputDataType } from "src/app/table-host/table-host.component";
import { Meritev } from "../Meritev";
import { Podatki, Postaja } from "./MeritveResponse";
import { ResponseBase } from "./ResponseBase";

export class PMDelciResponse extends ResponseBase {
    constructor(public podatkiPostaj: Postaja<PMDelciMeritev>[]) {
        super();
        //vsaki meritvi, ki jo dobimo iz backenda, dodamo še podatek o postaji in parametru, ker takšne podatke pričakuje backend
        podatkiPostaj.forEach((postaja) => {
            postaja.podatki.forEach(podatkipostaje => {
                podatkipostaje.meritve.forEach(meritevPodatka => {
                    meritevPodatka.parameter = podatkipostaje.parameterID;
                    meritevPodatka.datum_zajema = new Date(meritevPodatka.termin);
                    meritevPodatka.instalacija =postaja.id,
                    meritevPodatka.tip_meritve = TableInputDataType.PMDelci;//TODO tukaj je verjetno več različnih PM delcev in bo zato treba prilagodit. Najverjetneje bo treba ločit na pm10, pm25 ipd.
                })
            });
        });
    };
}

export interface PMDelciMeritev extends Meritev {
    podparameter: string;
    termin: string;
    vrednost: number;
    veljavnost_skz: number;
    volumen_pretoka: string;
    spodnja_meja_pretoka: string;
    masa: string;
    veljavnost_masa:string;
    veljavnost_kal:number;
    tip_merilnika:string;
    opomba:string;
    emep: string[];
}
