
export interface KljuciResponse {
    TipMeritve: string;
    OpisMeritve: string;
    Parametri: number[];
    Podparametri: string[];
    JsonKljuciVzorec: string[];
    JsonKljuciKontrola: string[];
}