import { ObserversModule } from "@angular/cdk/observers";
import { Type } from "@angular/core";
import { BehaviorSubject, Observable, Observer, Subject, Subscriber } from "rxjs";
import { InputDropdownComponent } from "../input-dropdown/input-dropdown.component";

export interface InputInfoController {
    inputInfoSubject: BehaviorSubject<InputInfo>;
}

export abstract class InputInfo implements InputInfoController {
    public component: Type<any> = InputDropdownComponent;
    public selectedValueSubject: BehaviorSubject<IInputInfoValue | undefined> = new BehaviorSubject<IInputInfoValue | undefined>(undefined);
    public inputInfoSubject: BehaviorSubject<InputInfo>;
    constructor(public name: string, public ID: string | null) {
        this.inputInfoSubject = new BehaviorSubject<InputInfo>(this)
        if (ID = null)
            ID = name;
    }
    public abstract getValues(): IInputInfoValue[];
    public getClickable(): boolean { return true };
    //emitting selectedValue change to all listeners
    public selectValue(newVal: IInputInfoValue) {
        this.selectedValueSubject.next(newVal);
    }

}
export class SimpleStringInputInfo extends InputInfo {
    public getValues(): IInputInfoValue[] {
        return this.dropdownStrings.map<IInputInfoValue>(val => (({ value: val }) as IInputInfoValue));
    }
    constructor(name: string, public dropdownStrings: String[], ID: string | null = null, clickable: boolean = true) {
        super(name, ID);
        this.getClickable = () => { return clickable };
    }
}

export class SimpleValueInputInfo extends InputInfo {
    public getValues(): IInputInfoValue[] {
        return this.dropDownValues;
    }
    constructor(name: string, public dropDownValues: IInputInfoValue[], ID: string | null = null, clickable: boolean = true) {
        super(name, ID);
        this.getClickable = () => { return clickable };
    }
}
export class ValueInputInfo extends InputInfo {
    public getValues(): IInputInfoValue[] {
        return this.dropdownValues;
    }
    constructor(name: string, public dropdownValues: IInputInfoValue[], ID: string | null = null) {
        super(name, ID);
    }
}
//II, ki nastane iz observable objekta. Ko observable objekt javi vrednost, se ta vrednost napolni v II
export class ObservingInputInfo extends InputInfo {
    private observedData: IInputInfoValue[] = [{ value: "Pridobivanje podatkov.." }];
    public getValues(): IInputInfoValue[] {
        return this.observedData;
    }
    constructor(name: string, private observableData: Observable<IInputInfoValue[]>, ID: string | null = null, public onDataArrivedFunction : ((arrivedData: IInputInfoValue[]) => void)| null = null) {
        super(name, ID);
        this.getClickable = () => { return false };
        observableData.subscribe({
            next: (data) => {
                if(onDataArrivedFunction != null)
                    onDataArrivedFunction(data);
                this.observedData = data; this.getClickable = () => { return true };
            },
            error: (data) => { this.observedData = [{ value: "Napak pri pridobivanju podatkov!" }] },
        });
    }
}

export class JoinedInputInfo extends InputInfo {
    //pridobimo zdruzene vrednosti vseh II
    //ce kateri izmed zdruzenih II se nima pripravljen (ni clickable) potem izpisemo njegove vrednosti (message)
    public getValues(): IInputInfoValue[] {
        var unclickable = this.getFirstUnclickable();
        if (unclickable != null)
            return unclickable.getValues();
        var valueArray: IInputInfoValue[] = [];
        this.IIs.forEach(ii => valueArray = [...valueArray, ...ii.getValues()]);
        return valueArray;
    }
    //ce so vsi clickable, je clickable tudi ta II
    public override getClickable() {
        return this.getFirstUnclickable() == null;
    }
    //vrne prvi II, ki ni clickable. ce so vsi clickable, returna null
    private getFirstUnclickable(): InputInfo | null {
        for (var i = 0; i < this.IIs.length; i++) {
            if (this.IIs[i].getClickable() == false)
                return this.IIs[i];
        }
        return null;
    }
    constructor(name: string, private IIs: InputInfo[], ID: string | null = null,) {
        super(name, ID);
    }
}

/**
 * funkcija, ki temelji na izboru (vrednosti) II
 * @date 5/27/2023 - 11:28:09 AM
 *
 * @typedef {CreateInputInfo}
 */
type CreateInputInfo = (providerInfo: IInputInfoValue) => InputInfo;


/**
 * Se uporablja kadar je en II odvisen od drugega
 * @date 5/27/2023 - 11:26:38 AM
 *
 * @export
 * @class DependingInputInfo
 * @typedef {DependingInputInfo}
 * @implements {InputInfoController}
 */
export class DependingInputInfo implements InputInfoController {
    public inputInfoSubject: BehaviorSubject<InputInfo>;
    private initInputInfo: CreateInputInfo;


    /**
     * Creates an instance of DependingInputInfo.
     * Ko II, ki je v parametru 'provider', generira vrednost, se s to vrednostjo kliče funkcija createInputInfoFunction. Rezultat funkcije je nov II, ki zamenja trenutnega
     * @date 5/27/2023 - 11:26:55 AM
     *
     * @constructor
     * @param {string} name //ime II
     * @param {InputInfo} provider //II od katerega je ta odvisen
     * @param {string} waitingMessage //sporočilo, ki se izpiše, preden je podatekv v provider II izbran
     * @param {CreateInputInfo} createInputInfoFunction //funkcija, ki se izvede, ko se v II izbere vrednost
     * @param {(string | null)} [ID=null] //id II
     */
    constructor(name: string, private provider: InputInfo, waitingMessage: string, createInputInfoFunction: CreateInputInfo, ID: string | null = null) {
        this.initInputInfo = createInputInfoFunction;
        //natavimo defaultno instanco s simpl sporocilom
        this.inputInfoSubject = new BehaviorSubject<InputInfo>(new SimpleStringInputInfo(name, [waitingMessage], ID, false));
        //provider je II od katerega smo odvisni. subscribamo na spremembo njegove vrednosti. 
        //ko se njegova vrednost spremeni, se sproži funkcija, ki smo jo definiral. 
        //rezultat funkcije je nov II, ki je sedaj naš nov II
        this.provider.selectedValueSubject.subscribe(async (providerInputInfoValue) => {
            if (providerInputInfoValue != undefined) {
                this.inputInfoSubject.next((new SimpleStringInputInfo(name, ["Pridobivanje podatkov.."], ID, false)))
                var newII = await this.initInputInfo(providerInputInfoValue!);
                this.inputInfoSubject.next(newII);
            } else {
                this.inputInfoSubject.next((new SimpleStringInputInfo(name, [waitingMessage], ID, false)))
            }
        });
    }



}

export interface IInputInfoValue {
    value: string;
    id?: string
}

