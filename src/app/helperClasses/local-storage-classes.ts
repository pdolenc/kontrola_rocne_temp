import { InjectionToken } from '@angular/core';

export const LOCAL_STORAGE = new InjectionToken<Storage>(
  'Shramba brskalnika',
  {
    providedIn: 'root',
    factory: () => localStorage
  }
);