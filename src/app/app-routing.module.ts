import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { IoniPahiComponent } from './glavneKomponente/ioni-pahi/ioni-pahi.component';
import { PmDelciComponent } from './glavneKomponente/pm-delci/pm-delci.component';
import { PrijavaComponent } from './prijava/prijava.component';
import { TableHostComponent } from './table-host/table-host.component';
import { TezkeKovineComponent } from './glavneKomponente/tezke-kovine/tezke-kovine.component';

const routes: Routes = [
  {path: '',component: HomePageComponent},
  {path: 'pmdelci',component: PmDelciComponent},
  {path: 'kovine',component: TezkeKovineComponent},
  {path: 'ionipahi',component: IoniPahiComponent},
  {path: 'test',component: TableHostComponent},
  {path: 'prijava',component:PrijavaComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
