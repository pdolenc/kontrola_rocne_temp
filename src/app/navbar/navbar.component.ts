import { Component, Input, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { DataRepositoryService } from '../services/data-repository.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Input() currentPage! : string;
  @Input() enableBackButton : boolean = true;

  userLoggedIn : boolean = false;
  userName? : string;

  constructor(private authService : AuthenticationService,private dataService: DataRepositoryService) { }

  ngOnInit(): void {
    this.userLoggedIn = this.authService.isUserLoggedIn();
    if(this.userLoggedIn)
      this.userName = this.authService.loggedUserData()?.ime;
  }

  odjava(){
    this.authService.logOut();
    this.userLoggedIn = false;
    this.userName = undefined;
  }

  ClearCache(){
    this.dataService.ClearCache()
  }
}
