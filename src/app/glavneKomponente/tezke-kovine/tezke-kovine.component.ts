import { HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { DependingInputInfo, IInputInfoValue, InputInfo, InputInfoController } from '../../helperClasses/input-info-classes';
import { IMeritevHandler } from '../meritev-handler';
import { InputCollectorComponent } from '../../input-collector/input-collector.component';
import { DataService } from '../../services/data.service';
import { InputInfoService } from '../../services/input-info.service';
import { ITableDataInput, TableHostComponent, TableInputDataType, TableMode } from 'src/app/table-host/table-host.component';
import { TableDirective } from 'src/app/table-host/table.directive';
import { CellPropertyDefinition, CellPropertyDefinitionCollection, ColumnDataWrapper, Stanje } from 'src/app/table-host/tableClasses';
import { DataRepositoryService } from 'src/app/services/data-repository.service';
import { firstValueFrom } from 'rxjs';
import { PadavineResponseBase } from 'src/app/models/Responses/Padavine';
import { AdditionalColumn, AdditionalColumnsTableMetaData } from 'src/app/models/TableMetadata';
import { ParametriResponse } from 'src/app/models/Responses/ParametriResponse';


@Component({
  selector: 'app-tezke-kovine',
  templateUrl: './tezke-kovine.component.html',
  styleUrls: ['../meritev-handler.css']
})
export class TezkeKovineComponent implements OnInit, IMeritevHandler {
  GatheringData: boolean = false;

  constructor(
    private IIS: InputInfoService,
    private dataService: DataService,
    private dataRepo: DataRepositoryService
  ) { }

  Mode: TableMode = TableMode.Import;
  handler: IMeritevHandler = this;
  @ViewChild(InputCollectorComponent) collector!: InputCollectorComponent;
  @ViewChild(TableDirective, { static: true }) tableHostDirective!: TableDirective;


  //exposing
  Modes = TableMode;
  displayData: boolean = false;

  //za prikaz error gumba, ce uporabnik se ni izbral vseh parametrov


  private letoII = this.IIS.LETO;
  public readonly inputs: InputInfoController[] = [
    this.letoII,
    new DependingInputInfo("Postaje", this.letoII, "Najprej izberite leto", (izbranoLeto) => this.IIS.GetPostajeInputInfo(`Postaje za leto ${izbranoLeto.value}`, "tedenske_pad_kovine_suh_use", new Date(Number.parseInt(izbranoLeto.value), 0, 1, 0, 0, 0, 0), new Date(Number.parseInt(izbranoLeto.value), 11, 31, 23, 59, 59, 0), '+07 00:00:00', "POSTAJA"))
  ];

  ngOnInit(): void {

  }

  ngAfterViewInit() {

  }

  //izberemo nacin prikaza podatkov (moznost urejanja ali samo pregled)
  public SelectMode(newMode: TableMode) {
    this.Mode = newMode;
  }

  //Preberemo uporabnikov input iz vseh InputInfo (dropdownov)
  //Glede na izbrane vrednosti InputInfo izvedemo klic za pridobivanje podatkov
  //Ko podatke pridobimo, naredimo in prikažemo interaktivno tabelo
  async receiveInputData(data: InputInfo[]): Promise<void> {
    //zastavica, da trenutno pridobivamo podatke
    this.GatheringData = true;

    ///pridobimo podatke iz InputInfo, ki jih je uporabnik izbral
    //podatek o letu spremenimo v api-ju primerno obliko
    var izbranoLeto = Number.parseInt(data.find(x => x.ID == 'LETO')!.selectedValueSubject.getValue()!.value!); //leto iz dropdown-a
    var datum_do = new Date(
      izbranoLeto,
      11, 31, 23, 59, 59, 0
    ).toJSON();
    var datum_od = new Date(
      izbranoLeto,
      0, 0, 0, 0, 0, 0
    ).toJSON();
    var postajaID = data.find(x => x.ID == 'POSTAJA')!.selectedValueSubject.getValue()!.id!

    //pridobivanje definicije za shemo teh vrst podatkov
    var shemaData = await this.dataRepo.GetKljuci("tedenske_pad_kovine_padavina");
    console.log(shemaData);
    //parametre sedaj dobivamo dinamično
    //var parametri = "9142,9143,9145,9146,9147,9148,9149,9150,9151,9152,9153,9154,9156,9157,9158,9160,9161,17323,17324,17325,17326,17327";

    //json polja se razlikujejo glede na bazo
    //var json_polja = this.Mode == TableMode.Import ? shemaData.JsonKljuciVzorec.join(',') : shemaData.JsonKljuciVzorec.join(',');
    //http parametri
    var httpParams = new HttpParams()
      .set('postaja', postajaID)
      .set('datum_od', datum_od)
      .set('datum_do', datum_do)
      .set('json_polja', shemaData.JsonKljuciVzorec.join(','))
      .set('parameter', shemaData.Parametri.join(','))
      .set('podparameter', 'koncentrac_suh_use,volumen_suh_use,koncentracija,volumen,veljavnost,depozicija');


    //Pri tezkih kovinah se na zacetku tabele dodajo še drugi podatki
    var masa = await firstValueFrom(this.dataService.GetPadavine("kal", postajaID, 604800, datum_od, datum_do));
    var visinaRocna = await firstValueFrom(this.dataService.GetPadavine("rocne", postajaID, 604800, datum_od, datum_do));
    var visinaAvt = await firstValueFrom(this.dataService.GetPadavine("avt", postajaID, 604800, datum_od, datum_do));
    //iz teh podatkov definiramo dodatne celice, ki bodo pripete tabeli
    var primarnaVrednostDodatniStolpci = new CellPropertyDefinition('vrednost', 'Vrednost')
    var dodatniStolpciCellProperties = new CellPropertyDefinitionCollection(
      [primarnaVrednostDodatniStolpci],
      primarnaVrednostDodatniStolpci,//primarna celica
      [],//samo za prikaz
      [] //special
    );
    var AdditionalColumns: AdditionalColumn<PadavineResponseBase[]>[] = [
      {
        ColumnDataWrapper: new ColumnDataWrapper('Masa vzorca'),
        ColumnInfo: masa,
        CellPropertyDefinition: dodatniStolpciCellProperties
      } as AdditionalColumn<PadavineResponseBase[]>,
      {
        ColumnDataWrapper: new ColumnDataWrapper('Količina padavin (ročna)'),
        ColumnInfo: visinaRocna,
        CellPropertyDefinition: dodatniStolpciCellProperties
      } as AdditionalColumn<PadavineResponseBase[]>,
      {
        ColumnDataWrapper: new ColumnDataWrapper('Količina padavin (avt.)'),
        ColumnInfo: visinaAvt,
        CellPropertyDefinition: dodatniStolpciCellProperties
      } as AdditionalColumn<PadavineResponseBase[]>,
    ];


    //pridobivanje podatkov iz backenda glede na izbran mode (nova ali stara baza)
    if (this.Mode == TableMode.EditOnlyEMEPAndValidity) {
      console.log(httpParams);

      //podatki iz nove baze
      this.dataService.getTezkeKovine(httpParams).subscribe(
        data => {
          //Prikazemo tabelo:
          //kreiranje nove komponente - tabele
          const viewContainerRef = this.tableHostDirective.viewContainerRef;
          viewContainerRef.clear();
          const componentRef = viewContainerRef.createComponent<TableHostComponent>(TableHostComponent);
          //novo kreirani tabeli podamo podatke za prikaz
          var gatheredData: ITableDataInput = {
            DataType: TableInputDataType.TezkeKovine,
            Parametri: shemaData.Parametri,
            Data: data,
            EmepEndpoint: "tedenske_pad_kovine",
            AdditionalEmepInfo: [{
              ID: 781,
              ForceStanje: Stanje.Vijolicno
            },
            {
              ID: 780,
              ForceStanje: Stanje.Oranzno
            }],
            TableMode: this.Mode,
            Metadata: {
              leto: izbranoLeto,
              additionalColumns: AdditionalColumns,
            } as TezkeKovineTableMetadata
          };
          componentRef.instance.DisplayData(gatheredData);
          //končali smo s pridobivanjem podatkov
          this.GatheringData = false;
        });
    } else if (this.Mode == TableMode.Import) {

      //podatki iz stare baze
      console.log(httpParams);
      this.dataService.getTezkeKovineFromRemoteDB(httpParams).subscribe(
        data => {
          //Prikazemo tabelo:
          //kreiranje nove komponente - tabele
          const viewContainerRef = this.tableHostDirective.viewContainerRef;
          viewContainerRef.clear();
          const componentRef = viewContainerRef.createComponent<TableHostComponent>(TableHostComponent);
          //novo kreirani tabeli podamo podatke za prikaz
          var gatheredData: ITableDataInput = {
            DataType: TableInputDataType.TezkeKovine,
            Parametri: shemaData.Parametri,
            Data: data,
            EmepEndpoint: "tedenske_pad_kovine",
            AdditionalEmepInfo: [{
              ID: 781,
              ForceStanje: Stanje.Vijolicno
            },
            {
              ID: 780,
              ForceStanje: Stanje.Oranzno
            }],
            TableMode: this.Mode,
            Metadata: {
              leto: izbranoLeto,
              additionalColumns: AdditionalColumns,
            } as TezkeKovineTableMetadata
          };
          componentRef.instance.DisplayData(gatheredData);
          //končali smo s pridobivanjem podatkov
          this.GatheringData = false;
        });
    }


  }




}
//dodatni podatki, ki jih tezke kovine potrebujejo za pravilen prikaz tabele
export interface TezkeKovineTableMetadata extends AdditionalColumnsTableMetaData<PadavineResponseBase[]> {
  leto: number;
}



