import { InputInfo } from "../helperClasses/input-info-classes";

export interface IMeritevHandler {
    receiveInputData(data : InputInfo[], kategorija : String) : void;
}
