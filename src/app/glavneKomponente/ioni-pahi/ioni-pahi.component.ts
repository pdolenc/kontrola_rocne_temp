import { Component, OnInit, ViewChild } from '@angular/core';
import { InputInfo, SimpleStringInputInfo } from '../../helperClasses/input-info-classes';
import { IMeritevHandler } from '../meritev-handler';
import { InputCollectorComponent } from '../../input-collector/input-collector.component';
import { InputInfoService } from '../../services/input-info.service';

@Component({
  selector: 'app-ioni-pahi',
  templateUrl: './ioni-pahi.component.html',
  styleUrls: ['../meritev-handler.css']
})
export class IoniPahiComponent implements OnInit,IMeritevHandler {

  constructor(private IIS : InputInfoService){}


  izbranaKategorija : Kategorija  = Kategorija.None;
  kategorije = Kategorija;
  handler : IMeritevHandler = this;
  @ViewChild(InputCollectorComponent) collector! : InputCollectorComponent;
  displayData : boolean = false;

  private readonly tedenskeInputs : InputInfo[] =[
    this.IIS.LETO,
    new SimpleStringInputInfo("Postaja",["TODO","PRIDE IZ SERVICE-a"])
  ];

  private readonly dnevneInputs : InputInfo[] =[
    this.IIS.LETO,
    new SimpleStringInputInfo("Postaja",["TODO","PRIDE IZ SERVICE-a"]),
    new SimpleStringInputInfo("Tedni",["TODO"]),
  ];

  ngOnInit(): void {

  }

  ngAfterViewInit() {
    this.collector.handler = this.handler;
  }

  izberiKategorijo( kat : Kategorija){ 
    if(kat == this.izbranaKategorija)
      return;
    this.izbranaKategorija = kat;
    if(kat == Kategorija.Depozicije_dnevne)
      this.collector.inputs = this.dnevneInputs;
    else if(kat == Kategorija.Koncentracije_dnevne)
      this.collector.inputs = this.dnevneInputs;
    else if(kat == Kategorija.Depozicije_tedenske)
      this.collector.inputs = this.tedenskeInputs;
    else if(kat == Kategorija.Koncentracije_tedenske)
      this.collector.inputs = this.tedenskeInputs;
    this.collector.name = kat.toString();
  }

  receiveInputData(data: InputInfo[], kategorija : String): void {
    console.log("kategorija: " +kategorija as Kategorija);
    console.log("Received " +data.length);
    this.displayData= true;
  }
}

enum Kategorija {
  None = '',
  Depozicije_dnevne = 'Depozicije - dnevne',
  Depozicije_tedenske = 'Depozicije - tedenske',
  Koncentracije_dnevne = 'Koncentracije - dnevne',
  Koncentracije_tedenske = 'Koncentracije - tedenske'
}