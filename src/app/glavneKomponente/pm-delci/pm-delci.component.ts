import { Component, Input, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { DependingInputInfo, IInputInfoValue, InputInfo, InputInfoController, JoinedInputInfo, ObservingInputInfo, SimpleStringInputInfo, SimpleValueInputInfo } from '../../helperClasses/input-info-classes';
import { IMeritevHandler } from '../meritev-handler';
import { InputCollectorComponent } from '../../input-collector/input-collector.component';
import { InputDropdownComponent } from '../../input-dropdown/input-dropdown.component';
import { InputInfoService } from '../../services/input-info.service';
import { TableDirective } from 'src/app/table-host/table.directive';
import { ITableDataInput, TableHostComponent, TableInputDataType, TableMode } from 'src/app/table-host/table-host.component';
import { DataService } from 'src/app/services/data.service';
import { DataRepositoryService } from 'src/app/services/data-repository.service';
import { HttpParams } from '@angular/common/http';
import { Stanje } from 'src/app/table-host/tableClasses';
import { firstValueFrom, Observable } from 'rxjs';

@Component({
  selector: 'app-pm-delci',
  templateUrl: './pm-delci.component.html',
  styleUrls: ['../meritev-handler.css']
})
export class PmDelciComponent implements OnInit, IMeritevHandler {

  GatheringData: boolean = false;

  constructor(
    private IIS: InputInfoService,
    private dataService: DataService,
    private dataRepo: DataRepositoryService
  ) { }

  Mode: TableMode = TableMode.Import;
  handler: IMeritevHandler = this;
  @ViewChild(InputCollectorComponent) collector!: InputCollectorComponent;
  @ViewChild(TableDirective, { static: true }) tableHostDirective!: TableDirective;

  //exposing
  Modes = TableMode;
  displayData: boolean = false;

  tipDelcevPM25IIValue: IInputInfoValue = { value: "PM2.5", id: "pm25" };
  tipDelcevPM10IIValue: IInputInfoValue = { value: "PM10", id: "pm10" };
  //ko imamo tip delcev, se more na backend naredit klic, ki vrne podatke za ta tip za analizo in koncentracijo.
  //tip, ki je parameter v klicu, je sestavljen iz id-ja tipa delcev (zgoraj^) in post-fix-ev, ki so:
  analizaPostfix = "_analiza";
  koncentracijaPostfix = "_koncentracija";



  onesnazevaloID = "Onesnaževalo";
  tipDelcevID = "TipDelcev";
  //logika koncentracije se razlikuje od ostalih onesnazeval. Koncentracija se prikaze neglede na izbran tip delcev.
  //ko iz backenda dobimo katere so vrednosti za koncentracijo, shranimo to v spemenljivko, da lahko nadaljno logiko mapiramo direktno na to spremenljivko
  koncentracija_onesnazevaloInputInfoValue!: IInputInfoValue[];
  tipDelcevII = new SimpleValueInputInfo("Tip PM delcev", [this.tipDelcevPM25IIValue, this.tipDelcevPM10IIValue], this.tipDelcevID);
  readonly inputs: InputInfoController[] = [
    this.IIS.LETO,
    this.IIS.MESEC,
    this.tipDelcevII,
    new DependingInputInfo("Onesnaževalo", this.tipDelcevII, "Najprej izberite tip delcev", val => this.pridobiOnesnazevalaZaTipDelcev(val), this.onesnazevaloID)
  ];


  //onesnazevala, ki jih prikazemo v II so odvisna od tipa delcev, ki smo ga izbrali
  private pridobiOnesnazevalaZaTipDelcev(selectedTipDelcev: IInputInfoValue): InputInfo {
    if (selectedTipDelcev == null)
      throw 'Napaka pri pridobivanju onesnazeval glede na tip!';

    console.log(this.IIS);
    //pridobimo II za koncentracijo in onesnazevala za izbran tip
    var iikoncentracija = this.IIS.GetParametriInputInfo("OnesnaževaloKoncentracija", this.onesnazevaloID, `${selectedTipDelcev.id}${this.koncentracijaPostfix}`,(data) => {this.koncentracija_onesnazevaloInputInfoValue = data})
    var iianaliza = this.IIS.GetParametriInputInfo("OnesnaževaloAnaliza", this.onesnazevaloID, `${selectedTipDelcev.id}${this.analizaPostfix}`);
    //pri koncentraciji je logika klica za podatke drugacna. Zato si shranimo katere izmed vrednosti dropdowna predstavljajo koncentracijo
    console.log({konc: iikoncentracija, anal: iianaliza});
    // II zdruzimo v enega
    return new JoinedInputInfo("Onesnaževalo", [iikoncentracija, iianaliza], this.onesnazevaloID);

  }

  ngOnInit(): void {

  }

  ngAfterViewInit() {
    this.collector.handler = this.handler;
  }



  //Preberemo uporabnikov input iz vseh InputInfo (dropdownov)
  //Glede na izbrane vrednosti InputInfo izvedemo klic za pridobivanje podatkov
  //Ko podatke pridobimo, naredimo in prikažemo interaktivno tabelo
  async receiveInputData(data: InputInfo[]): Promise<void> {
    //zastavica, da trenutno pridobivamo podatke
    this.GatheringData = true;

    //pridobivanje podatkov je ločeno glede na podatke, ki so izbrani na input info.
    var tipMeritve: string;
    //in sicer od "Tip PM delcev"
    var tipPmDelcev = data.find(x => x.ID == this.tipDelcevID)!.selectedValueSubject.getValue()!.id!;
    //in od "Onesnazevalo"
    var izbranoOnesnazevalo = data.find(x => x.ID == this.onesnazevaloID)!.selectedValueSubject.getValue();
    console.log({ tipPmDelcev, parameter: izbranoOnesnazevalo });

    //ce imamo izbrano onesnazevalo 'koncentracija':
    //console.log(this.koncentracija_onesnazevaloInputInfoValue);
    if (this.koncentracija_onesnazevaloInputInfoValue.includes(izbranoOnesnazevalo!)) {
      tipMeritve = `${tipPmDelcev}${this.koncentracijaPostfix}`;
    }
    //v vseh ostalih primerih:
    else { 
      tipMeritve = `${tipPmDelcev}${this.analizaPostfix}`;
    }

    //console.log("tip meritve: " + tipMeritve);

    //pridobivanje definicije za shemo teh vrst podatkov
    var shemaData = await this.dataRepo.GetKljuci(tipMeritve);
    //console.log(shemaData);
    //json polja se razlikujejo glede na bazo
    //izbran mesec/leto je treba prilagoditi api-ju
    var izbranoLeto = data.find(x => x.ID == 'LETO')!.selectedValueSubject.getValue()!.value!;
    var izbranMesec = data.find(x => x.ID == 'MESEC')!.selectedValueSubject.getValue()!.value!;
    var mesecIndex = this.IIS.GetMonthNumber(izbranMesec)!;
    const [startDate, nextMonthStartDate] = this.createDates(parseInt(izbranoLeto, 10), mesecIndex);

    //potrebujemo podatke za vse postaje
    var postaje = this.dataRepo.GetPostaje(tipMeritve, new Date(Number.parseInt(izbranoLeto), 0, 1, 0, 0, 0, 0), new Date(Number.parseInt(izbranoLeto), 11, 31, 23, 59, 59, 0), '+01 00:00:00');//TODO spremeni/preveri trajanje
    if (postaje instanceof Observable)
      postaje = await firstValueFrom(postaje);

    console.log(postaje);
    //http parametri


    //pridobivanje podatkov iz backenda glede na izbran mode (nova ali stara baza)
    if (this.Mode == TableMode.EditOnlyEMEPAndValidity) {

      var httpParams = new HttpParams()
      .set('postaja', postaje.map(postaja => postaja.id).join(','))
      .set('datum_od', startDate.toJSON())
      .set('datum_do', nextMonthStartDate.toJSON())
      .set('json_polja', shemaData.JsonKljuciVzorec.join(','))
      .set('parameter', izbranoOnesnazevalo?.id!) //pri PM delcih je parameter definiran glede na izbrano onesnaževalo
      .set('podparameter', shemaData.Podparametri.join(','));


      //podatki iz nove baze
      this.dataService.getPMDelci(httpParams).subscribe(
        data => {
          //Prikazemo tabelo:
          //kreiranje nove komponente - tabele
          const viewContainerRef = this.tableHostDirective.viewContainerRef;
          viewContainerRef.clear();
          const componentRef = viewContainerRef.createComponent<TableHostComponent>(TableHostComponent);
          //novo kreirani tabeli podamo podatke za prikaz
          var gatheredData: ITableDataInput = {
            DataType: TableInputDataType.PMDelci,
            Parametri: [Number.parseInt(izbranoOnesnazevalo!.id!)],
            Data: data,
            EmepEndpoint: "pm",
            AdditionalEmepInfo: [],
            TableMode: this.Mode,
            Metadata: {
              mesec: mesecIndex,
              leto: izbranoLeto
            }
          };
          componentRef.instance.DisplayData(gatheredData);
          //končali smo s pridobivanjem podatkov
          this.GatheringData = false;
        });
    } else if (this.Mode == TableMode.Import) {
      var httpParams = new HttpParams()
      .set('postaja', postaje.map(postaja => postaja.id).join(','))
      .set('datum_od', startDate.toJSON())
      .set('datum_do', nextMonthStartDate.toJSON())
      //.set('json_polja', json_polja)
      .set('parameter', izbranoOnesnazevalo?.id!) //pri PM delcih je parameter definiran glede na izbrano onesnaževalo
      ;
      //.set('podparameter', shemaData.Podparametri.join(','));
      //podatki iz stare baze
      this.dataService.getPMDelciFromRemoteDB(httpParams).subscribe(
        data => {
          //Prikazemo tabelo:
          //kreiranje nove komponente - tabele
          const viewContainerRef = this.tableHostDirective.viewContainerRef;
          viewContainerRef.clear();
          const componentRef = viewContainerRef.createComponent<TableHostComponent>(TableHostComponent);
          //novo kreirani tabeli podamo podatke za prikaz
          var gatheredData: ITableDataInput = {
            DataType: TableInputDataType.PMDelci,
            Parametri: [Number.parseInt(izbranoOnesnazevalo!.id!)],
            Data: data,
            EmepEndpoint: "pm",
            AdditionalEmepInfo: [],
            TableMode: this.Mode,
            Metadata: {
              mesec: mesecIndex,
              leto: izbranoLeto
            }
          };
          componentRef.instance.DisplayData(gatheredData);
          //končali smo s pridobivanjem podatkov
          this.GatheringData = false;
        });
    }


  }

  //glede na izbrano leto in mesec vrnemo datum, ki je primeren za API - torej prvi dan izbranega mesca in prvi dan naslednjega mesca
  public createDates(year: number, month: number): [Date, Date] {
    // Create the start of the selected month
    const startDate = new Date(year, month - 1, 1);

    // Create the start of the next month
    const nextMonth = month === 12 ? 1 : month + 1;
    const nextYear = month === 12 ? year + 1 : year;
    const nextMonthStartDate = new Date(nextYear, nextMonth - 1, 1);

    return [startDate, nextMonthStartDate];
  }

  //izberemo nacin prikaza podatkov (moznost urejanja ali samo pregled)
  public SelectMode(newMode: TableMode) {
    this.Mode = newMode;
  }
}




