import { InjectionToken, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { PmDelciComponent } from './glavneKomponente/pm-delci/pm-delci.component';
import { InputDropdownComponent } from './input-dropdown/input-dropdown.component';
import { InputCollectorComponent } from './input-collector/input-collector.component';
import { TezkeKovineComponent } from './glavneKomponente/tezke-kovine/tezke-kovine.component';
import { IoniPahiComponent } from './glavneKomponente/ioni-pahi/ioni-pahi.component';
import { TableHostComponent } from './table-host/table-host.component';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { TooltipModule } from 'ng2-tooltip-directive';
import { RowModalContentComponent } from './table-host/row-modal-content/row-modal-content.component';
import { CellModalContentComponent } from './table-host/cell-modal-content/cell-modal-content.component';
import { FormsModule } from '@angular/forms';
import { PrijavaComponent } from './prijava/prijava.component';
import { JwtModule } from "@auth0/angular-jwt";
import { DataRepositoryService } from "./services/data-repository.service";
import { NavbarComponent } from './navbar/navbar.component';
import { TableDirective } from './table-host/table.directive';
import { TablePopupDisplayComponent } from './table-host/popup-display/table-popup-display.component'
import { TippyModule, tooltipVariation, popperVariation } from '@ngneat/helipopper';
import { RowModalPopupDisplayComponent } from './table-host/row-modal-content/row-modal-popup-display/row-modal-popup-display.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { SelectRowsModalComponent } from './table-host/select-rows-modal/select-rows-modal.component';
import { GraphModalContentComponent } from './table-host/graph-modal-content/graph-modal-content.component';
import { NgChartsModule } from 'ng2-charts';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { ApplicationSettings } from './settings/appSettings';
import { APP_SETTINGS } from './settings/appSettings';

export const AppSettings = new InjectionToken<ApplicationSettings>('base environment settings of application');


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    PmDelciComponent,
    InputDropdownComponent,
    InputCollectorComponent,
    TezkeKovineComponent,
    IoniPahiComponent,
    TableHostComponent,
    RowModalContentComponent,
    CellModalContentComponent,
    PrijavaComponent,
    NavbarComponent,
    TableDirective,
    TablePopupDisplayComponent,
    RowModalPopupDisplayComponent,
    SelectRowsModalComponent,
    GraphModalContentComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatTableModule,
    MatSortModule,
    BrowserAnimationsModule,
    NgbModule,
    HttpClientModule,
    TooltipModule,
    FormsModule,
    MatCheckboxModule,
    NgChartsModule,
    MatProgressSpinnerModule,
    TippyModule.forRoot({
      defaultVariation: 'tooltip',
      variations: {
        tooltip: {
          arrow: true,
          animation: false,
          delay: [0,0],
          trigger: 'mouseenter',
          offset: [0, 0],
        },
        popper: popperVariation,
      }
    })
  ],
  providers: [{
    provide: AppSettings,
    useValue:APP_SETTINGS,
  }],
  bootstrap: [AppComponent],
})
export class AppModule { }
