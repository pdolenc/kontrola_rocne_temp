import { environment } from "src/environments/environment";
export interface Endpoints  {
    [index: string]: string
}

export interface ApplicationSettings {
    DataServiceSettings? : DataServiceSettings;
}
export interface DataServiceSettings {
    BaseUrl?: string;
    Endpoints?: Endpoints;

}


//note: to so common settingi. Lahko se povozijo glede na environment
const APP_SETTINGS_BASE : ApplicationSettings = {
    DataServiceSettings : {
        BaseUrl: "",
        Endpoints: {
                GetPadavine: "padavine",
                getParameters: "parametri",
                getPostaje: "postaje",
                getParametri: "parametri",
                getTedniVLetu: "tedni",
                getPMDelci: "podatki-kontrola",
                getPMDelciFromRemoteDB: "podatki-pm-kal",
                getTezkeKovine: "podatki-kontrola",
                getTezkeKovineFromRemoteDB: "podatki-kal",
                getEmepData: "zastavice",
                UpdateCellsInDB: "update_emep/",
                insertData: "insert_data",
                getKljuci: "kljuci-json",
        }
    }
};

//app settingi trenutnega environmenta (povozoijo common settinge)
export const APP_SETTINGS : ApplicationSettings ={
    DataServiceSettings: {
        BaseUrl: environment.ApplicationSettings?.DataServiceSettings?.BaseUrl || APP_SETTINGS_BASE.DataServiceSettings?.BaseUrl,
        Endpoints: {
            ...APP_SETTINGS_BASE.DataServiceSettings?.Endpoints,
            ...environment.ApplicationSettings?.DataServiceSettings?.Endpoints
        }
    }
};


