import { Component, Inject, Input, OnInit } from '@angular/core';
import { PopupData } from '../tableClasses';
import { TIPPY_REF, TippyInstance } from '@ngneat/helipopper';
import { DataService, EMEPData } from 'src/app/services/data.service';
import { DataRepositoryService } from 'src/app/services/data-repository.service';

@Component({
  selector: 'app-popup-display',
  templateUrl: './table-popup-display.component.html',
  styleUrls: ['./table-popup-display.component.css']
})
export class TablePopupDisplayComponent implements OnInit {

  @Input()
  DataToDisplay?: PopupData;
  @Input()
  TablePopupDisplayMode! : TablePopupDisplayMode;
  TablePopupDisplayModes = TablePopupDisplayMode;
  constructor(
    @Inject(TIPPY_REF) tippy: TippyInstance,
    private localStorage: DataRepositoryService) {
    this.DataToDisplay = tippy.data?.PopupData;
    this.TablePopupDisplayMode = tippy.data.TablePopupDisplayMode ?? TablePopupDisplayMode.Full;
  }

  ngOnInit(): void {
  }

  getEMEPStyle(emep : EMEPData){
    return `color:${emep.DisplayColor}`;
  }
}

export enum TablePopupDisplayMode
{
  Full,                               //Prikazemo vse podatke
  EMEPOnly,                           //prikazemo samo EMEP podatke
  DisplayPropertiesOnly               //prikazemo samo podatke, ki so samo za prikaz (komentarji)
}
