import { LiveAnnouncer } from '@angular/cdk/a11y';
import { CdkNoDataRow } from '@angular/cdk/table';
import { KeyValue } from '@angular/common';
import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, NgModule, TemplateRef, Type, ViewChild, ViewContainerRef, ViewEncapsulation } from '@angular/core';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { lastValueFrom } from 'rxjs';
import { CellModalContentComponent, CellModalContentMode } from './cell-modal-content/cell-modal-content.component';
import { ResponseBase } from '../models/Responses/ResponseBase';
import { TedenData } from '../models/Responses/TedniVLetuResponse';
import { TezkeKovineMeritev, TezkeKovineResponse } from '../models/Responses/TezkeKovineResponse';
import { RowModalContentComponent, RowModalContentMode } from './row-modal-content/row-modal-content.component';
import { DataService, EMEPData } from '../services/data.service';
import { TablePopupDisplayComponent } from './popup-display/table-popup-display.component';
import { TableDirective } from './table.directive';
import { utils, WorkBook, WorkSheet, writeFile } from 'xlsx';
import { SelectRowsModalComponent } from './select-rows-modal/select-rows-modal.component';
import { DataRepositoryService } from '../services/data-repository.service';
import { FullGraphData, GraphDataWrapper, GraphModalContentComponent, GraphUnit } from './graph-modal-content/graph-modal-content.component';
import { CellDataWrapper, CellPropertyDefinition, CellPropertyDefinitionCollection, CellValues, ColumnDataWrapper, DisplayTedenDataWrapper, RowDataWrapper, SimpleDisplayDataWrapper, SingleRowDataWrapper, Stanje, Status, TableDataWrapper } from './tableClasses';
import { InsertMeritev, InsertMeritevRequest } from '../models/Requests/InsertTezkeKovineRequest';
import { PMDelciMeritev, PMDelciResponse } from '../models/Responses/PMDelciResponse';
import { formatDate } from '@angular/common';
import { Meritev } from '../models/Meritev';
import { TezkeKovineTableMetadata } from '../glavneKomponente/tezke-kovine/tezke-kovine.component';
import { PadavineResponseBase } from '../models/Responses/Padavine';
import { UpdateMeritev } from '../models/Requests/UpdateMeritveRequest';



@Component({
  selector: 'app-table-host',
  styleUrls: ['table-host.component.css'],
  templateUrl: './table-host.component.html',
  //  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class TableHostComponent {

  //expose to view
  TableMode = TableMode;

  //tabelo refreshamo on push
  public OnPushCDS = ChangeDetectionStrategy.OnPush;
  //ali je tabela že naložena. Nastavi se na true ob prvem dobivanju podatkov
  public initialized: boolean = false;
  //podatki,ki definirajo tabelo.
  public inputData!: ITableDataInput;
  //vsi podatki, ki jih potrebujemo za kreiranje tabele
  //vsakič, kodobimo nove podatke, osvežimo tabelo z novimi podatki
  @Input()
  public async DisplayData(inputdata: ITableDataInput) {
    console.log("new data arrived to table");
    this.inputData = inputdata;
    //vsi podatki tabele
    await this.transformInputData(inputdata).then((transformedTableData) => {
      this.tableData = transformedTableData;
      //console.log({notCompletelyEmptyRows: this.tableData.NonCompletelyEmptyRows});
      this.source = new MatTableDataSource(this.tableData.NonCompletelyEmptyRows)
      this.initialized = true;
    });

    //this..detectChanges();

  }

  @ViewChild("Table", { static: false }) Tabela!: ElementRef;
  //vsi podatki, ki jih potrebujemo za kreiranje tabele
  public tableData!: TableDataWrapper;
  //definicije stolpcev
  get allColumns(): ColumnDataWrapper[] {
    return this.tableData.allColumns;
  }
  get staticColumns(): ColumnDataWrapper[] {
    return this.tableData.StaticColumns;
  }
  get variableColumns(): ColumnDataWrapper[] {
    return this.tableData.VariableColumns;
  }
  get additionalColumns(): ColumnDataWrapper[] {
    if (this.tableData.AdditionalColumns == null)
      return [];
    return this.tableData.AdditionalColumns;
  }

  get columnsForDisplay(): ColumnDataWrapper[] {
    if (this.tableData.AdditionalColumns != null)
      return [...this.tableData.AdditionalColumns, ...this.tableData.VariableColumns];
    else
      return this.tableData.VariableColumns;
  }
  get columnIDs(): string[] {
    return this.allColumns.map<string>(coldata => coldata.Id);
  }
  get columnNames(): string[] {
    return this.allColumns.map<string>(col => col.DisplayName);
  }
  //ime tabele
  get tableName(): string {
    return this.tableData.Name;
  }
  get tableSubname(): string | null {
    return this.tableData.Subname;
  }
  get allCells(): CellDataWrapper[] {
    var allCells: CellDataWrapper[] = [];
    this.tableData.AllRows.forEach(row => {
      allCells = allCells.concat(row.PrimaryRow.GetAllCells());
      row.SecondaryRows?.forEach(secRow => allCells = allCells.concat(secRow.GetAllCells()));
    });
    return allCells;
  }

  //podatki za sortiranje. Mapira leto v njemu pripadajoče tedne
  private static tedniVLetu: Map<number, TedenData[]> = new Map<number, TedenData[]>();
  private async getTedniVLetu(leto: number): Promise<TedenData[]> {
    if (TableHostComponent.tedniVLetu.get(leto) == undefined) {
      return (await lastValueFrom(this.dataService.getTedniVLetu(leto)))?.tedni
    } else {
      return TableHostComponent.tedniVLetu.get(leto)!;
    }
  }

  //dejanski podatki, ki bojo v tabeli.
  //v to obliko pretvorimo podatke iz rawData, ki jih dobimo kot input
  source!: MatTableDataSource<any>;



  constructor(
    public modalService: NgbModal,
    private _liveAnnouncer: LiveAnnouncer,
    private dataService: DataService,
    private dataRepository: DataRepositoryService,
    //private ChangeDetector: ChangeDetectorRef
  ) {
  }


  //komponenta za popup na celicah
  comp = TablePopupDisplayComponent

  //ali se vrstica prikaze v tabeli
  public IsStaticColumnDisplayed(colID: string): boolean {
    return colID != null && this.staticColumns.find(col => col.Id == colID) != null;
  }

  //sortiranje
  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }
  //select/deselect vseh vrstic
  SelectAllRows(bool: boolean) {
    this.tableData.PrimaryRows.forEach(row => row.Select(bool));
  }

  //metoda se izvede ob kliku na celico
  editCellClick(rowData: SingleRowDataWrapper, cellData: ColumnDataWrapper) {
    console.log(rowData.ClusterRowData);
    const cellModal = this.modalService.open(CellModalContentComponent, { size: 'lg' });
    cellModal.componentInstance.rowData = rowData.ClusterRowData;
    cellModal.componentInstance.columnData = cellData;
    cellModal.componentInstance.emepEndpoint = this.inputData.EmepEndpoint;
    cellModal.componentInstance.additionalEmepInfo = this.inputData.AdditionalEmepInfo;
    if (this.inputData.TableMode == TableMode.Import)
      cellModal.componentInstance.mode = CellModalContentMode.DisplayOnly;
    if (this.inputData.TableMode == TableMode.EditOnlyEMEPAndValidity)
      cellModal.componentInstance.mode = CellModalContentMode.EditOnlyEMEP;
    if (this.inputData.TableMode == TableMode.Edit)
      cellModal.componentInstance.mode = CellModalContentMode.StandaloneEdit;

    cellModal.result.then(
      (dismissed) => { console.log("Dismissed modal: " + dismissed) },
      (reason) => {
        console.log(`Dismissed cell ${this.getDismissReason(reason)}`);
      }
    );
  }
  //metoda se izvede ob kliku na gumba za urejanje vrstice
  editRowClick(row: RowDataWrapper) {
    if (this.inputData.TableMode == TableMode.Import) {
      //za ziher. Naceloma so gumbi, ki omogocajo klic te metode itak onemogoceni, kadar smo v Import mode-u
      return;
    }
    //odpremo modal
    const rowModal = this.modalService.open(RowModalContentComponent, { size: 'xl' });
    //mu podamo podatke
    rowModal.componentInstance.RowData = row;
    rowModal.componentInstance.emepEndpoint = this.inputData.EmepEndpoint;
    rowModal.componentInstance.additionalEmepInfo = this.inputData.AdditionalEmepInfo;
    if (this.inputData.TableMode == TableMode.EditOnlyEMEPAndValidity)
      rowModal.componentInstance.mode = RowModalContentMode.EditOnlyEMEPAndValidity;
    else if (this.inputData.TableMode == TableMode.Edit)
      rowModal.componentInstance.mode = RowModalContentMode.Edit;
    else
      rowModal.componentInstance.mode = RowModalContentMode.DisplayOnly;

  }

  //pridobimo stil gumba za vsako stanje
  getStatusColor(status: Status) {
    switch (status) {
      case Status.Nespremenjeno:
        return 'btn-light';
      case Status.Spremenjeno:
        return 'btn-info';
      case Status.Spremenjeno:
        return 'btn-success';
      default:
        return 'btn-danger';
    }
  }

  //pridobimo stil (okvirja  neke celice)
  public GetCellStyle(cell: CellDataWrapper) {
    return cell?.veljavnost == true ? 'cell-bg-veljaven' : 'cell-bg-neveljaven';
  }

  //funkcija se kliče ob zapiranju modala in nam pove na kakšen način je uporabnik modal zaprl
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  //odpiranje modala za export v excel
  public exportToExcelClick() {
    const selectRowsModal = this.modalService.open(SelectRowsModalComponent, { size: 'md' });
    selectRowsModal.componentInstance.Title = "Izvoz v Excel";
    selectRowsModal.componentInstance.NumberOfRows = this.tableData.NonEmptyPrimaryRows.length;
    selectRowsModal.componentInstance.NumberOfSelectedRows = this.tableData.NonEmptyPrimaryRows.filter(row => row.Selected == true).length;
    selectRowsModal.result.then(
      (onlySelectedRows) => {
        this.ExportTableToExcel(onlySelectedRows);
      },
      (reason) => {
        console.log(`Dismissed modal with reason: ${this.getDismissReason(reason)}`);
      }
    );
  }

  //kreiranje excela
  public ExportTableToExcel(onlySelectedRows: boolean) {
    //preberemo propertyje vrednosti
    var properties = this.tableData.PropertyDefinitions.PropertyDefinitions;

    //vpisemo parametre
    let dataRows = [["Teden", "Podparameter", ...this.variableColumns.map(columnData => `${columnData.DisplayName} - ${columnData.Id}`)]];
    //vpisemo podatke

    (this.source.data as SingleRowDataWrapper[]).forEach(
      row => {
        if (onlySelectedRows == true && row.Selected == false)
          return;
        //natavimo prva dva fixna
        properties.forEach(valueProperty => {
          let rowArray = [row.Name.DisplayName, valueProperty.PropertyDisplayName];
          this.variableColumns.forEach(col => {
            rowArray.push(row.getVariableColumnCellData(col)?.Value![valueProperty.Property] ?? "ni podatka")
          });
          dataRows.push(rowArray);
        });
      });
    var worksheet = utils.aoa_to_sheet(dataRows);
    const wb: WorkBook = utils.book_new();
    utils.book_append_sheet(wb, worksheet, `Izvozeni podatki - ${new Date().toLocaleDateString()}`);
    writeFile(wb, `Izvozeni podatki - ${new Date().toLocaleDateString()}.xlsx`);

  }


  //da se property-ji pokazejo v pravem vrstnem redu
  public keepOriginalOrder = (a: any, b: any) => a.key

  //spremembe vrstice shranimo v bazo
  public saveRowDataClick(data: SingleRowDataWrapper) {
    console.log("Saving row data");

  }

  private removeEmptyEmep(emepData: [EMEPData, EMEPData, EMEPData]): EMEPData[] {
    return (structuredClone(emepData) as [EMEPData, EMEPData, EMEPData]).filter(flag => flag.ID != null);
  }



  //prejete podatke spremenimo v obliko, ki je primerna za prikaz tabele
  private async transformInputData(inputData: ITableDataInput): Promise<TableDataWrapper> {
    switch (inputData.DataType) {
      case TableInputDataType.TezkeKovine:
        var data = inputData.Data as TezkeKovineResponse;
        //console.log(JSON.stringify(data,null,2));

        //TODO reponse je verjetno skor vedno iste oblike, ne? Tako da bi lahko te podatke dobil iz ResponseBase in ne TezkeKovineResponse
        //v requestih posljemo samo parametre v obliki id-jev.
        //v responsu dobimo za vsak parameter tudi ime.
        //pridobimo polne podatke stolpcev
        var variabilniStolpci = inputData.Parametri.map<ColumnDataWrapper>(parameter => {
          var parameterFull = data?.podatki?.find(podatki => podatki.parameterID == parameter);
          if (parameterFull == undefined)
            return new ColumnDataWrapper(`Neznan parameter ${parameter}`, parameter);
          else
            return new ColumnDataWrapper(parameterFull.parameterIme, parameterFull.parameterID);
        });
        //nastavimo staticne stolpce
        var staticniStolpci = [
          new ColumnDataWrapper('Pozicija'),
          new ColumnDataWrapper('Izbranost'),
        ];

        var metadata = inputData.Metadata as TezkeKovineTableMetadata;


        //ce smo v nacinu urejanja podatkov, potem dodamo se "edit row" stolpec (na drugo mesto)
        if (inputData.TableMode != TableMode.Import)
          staticniStolpci.splice(1, 0, new ColumnDataWrapper("EditRowButton"));
        //pridobimo EMEP zastavice
        //TODO ni logično, da je delna logika (additionalEmepData) v ITableDataInput, ostala (pridobivanje emep zastavic) pa tukaj.
        //TODO bolje bi blo, da se to vse prestavi sem in je ITableDataInput namenjen izključno podatkom, ki jih uporabnik izbere na zaslonski maski
        var allEmepData = await this.dataRepository.GetEmepData(inputData.EmepEndpoint, inputData.AdditionalEmepInfo);

        ///vrstice
        //pridobimo vse tedne v določenem letu
        var tedniVLetu = await this.getTedniVLetu(metadata.leto); //todo: verjetno bi bilo bolje, da se ta klic izvede ob klicih za pridobivanje podatkov

        //definiramo glavne podatke ( property-je celic).
        //definiramo tudi podatke, ki so samo za prikaz
        //prva je primarna
        var primarnaVrednost = new CellPropertyDefinition('depozicija', 'Depozicija')
        var cellProperties: CellPropertyDefinitionCollection = new CellPropertyDefinitionCollection(
          [
            primarnaVrednost,
            new CellPropertyDefinition('koncentracija', 'Koncentracija'),
            new CellPropertyDefinition('koncentrac_suh_use', 'Koncentracija - suhe usedline')
          ],
          primarnaVrednost,
          [
            new CellPropertyDefinition('opomba_lab', 'Opomba laboratorija'),
            new CellPropertyDefinition('opomba_merilno_mesto', 'Opomba merilnega mesta'),
            new CellPropertyDefinition('komentar', 'komentar SKZ')
          ],
          [
            new CellPropertyDefinition('komentar', 'komentar SKZ')
          ]
        );
        //vrstice so meritve posameznega tedna
        var rows = tedniVLetu.map<RowDataWrapper>(
          (teden, index) => {

            //pri mapiranju tednov s podatki mapiramo teden.datumOd (torej zacetek) s podatkom parametrov. V parametrih pa je zapisan konec merjenja.
            //Zato moramo od tedna odstet 7 dni (oziroma kar trajanje v sekundah, ker je v takem formatu tudi zapisano v parametrih), da dobimo pravilni zacetek tedna
            var zacetekTedna = new Date(teden.datumOd);
            zacetekTedna.setSeconds(zacetekTedna.getSeconds() - 604800);

            //naredimo osnovni ovoj vrstic kateremu bomo kasneje nastavili podatke
            var vseVrsticeTedna: RowDataWrapper = new RowDataWrapper(cellProperties, new DisplayTedenDataWrapper(teden.tedenStevilka, teden.datumOd, teden.datumDo), variabilniStolpci, staticniStolpci, metadata.additionalColumns.map(addC => addC.ColumnDataWrapper));
            //primarna vrstica
            var primaryRow = new SingleRowDataWrapper(
              vseVrsticeTedna,     //gruča vseh vrstic h kateri spada ta
              teden.tedenStevilka,               //index vrstice
              'Tezke kovine'         //vrsta podatka
            );


            //v podatke vrstice primarne vrstice vpišemo vrednosti za vsak stolpec posebej
            //TODO nekateri podatki imajo drugo trajanje za isti teden. A mapiram se trajanje (torej da kot ustrezne gledam samo rezultate, ki imajo trajanje en teden)?
            variabilniStolpci.forEach(parameter => {
              var podatkiParametra = data.podatki?.find(podatek => podatek.parameterID.toString() == parameter.Id && podatek.meritve?.find(meritev => meritev.termin == teden.datumOd && meritev.trajanje == 604800 && meritev.veljavnost_skz != 99) != null)?.meritve?.find(meritev => meritev.termin == teden.datumOd && meritev.trajanje == 604800 && meritev.veljavnost_skz != 99);
              if (podatkiParametra != null) {
                var emepData = podatkiParametra?.emep?.split(',')?.map<number>(emepZastavica => Number.parseInt(emepZastavica)).map<EMEPData>((number) => {
                  var zastavica = allEmepData.find(emepData => emepData.ID == number)!
                  if (zastavica == null) {
                    console.error(`Zastavica z ID ${number} ni definirana!`);
                    //TEMP FIX
                    zastavica = EMEPData.Unknown;
                  }
                  return zastavica!;
                });
                if (emepData == null)
                  emepData = [];

                //console.log(JSON.stringify(podatkiParametra,null,2));
                //vcasih je potreben izracun depozicije (ker ne pride iz backenda)
                if (inputData.TableMode == TableMode.Import)
                  podatkiParametra.depozicija = this.izracunDepozicije(
                    podatkiParametra.volumen,
                    podatkiParametra.koncentracija,
                    podatkiParametra.volumen_suh_use,
                    podatkiParametra.koncentrac_suh_use,
                    Number.parseFloat(podatkiParametra.tkd_premer_lijaka));

                //glede na definirane glavne podatke celice in podatke, ki so samo za izpis, generiramo dinamičen class, ki drži
                //te nam pomembne vrednosti
                var cellValues = {} as CellValues;
                //v vrednosti celice prepišemo nam pomembne glavne podatke iz meritev
                cellProperties.PropertyDefinitions.forEach(propDef => {
                  var key = propDef.Property as keyof TezkeKovineMeritev;
                  cellValues[propDef.Property] = podatkiParametra![key]?.toString();
                });
                //v vrednosti celice prepisemo nam pomembne podatke za izpis iz meritev
                cellProperties.DisplayPropertyDefinitions.forEach(displayPropDef => {
                  var key = displayPropDef.Property as keyof TezkeKovineMeritev;
                  cellValues[displayPropDef.Property] = podatkiParametra![key]?.toString();
                });
                //v vrednosti celice prepisemo posebne podatke
                cellProperties.SpecialPropertyDefinitions.forEach(specPropdef => {
                  var key = specPropdef.Property as keyof TezkeKovineMeritev;
                  cellValues[specPropdef.Property] = podatkiParametra![key]?.toString();
                });
                //preberemo in v vrstico zapisemo podatke o depoziciji
                primaryRow.Values[parameter.Id!] = new CellDataWrapper(
                  data.id,
                  inputData.DataType,
                  podatkiParametra,
                  parameter,
                  cellProperties,
                  cellValues,
                  [emepData[0], emepData[1], emepData[2]],
                  podatkiParametra.veljavnost_skz == 1,
                  //za test vzamemo kar to
                  false
                )
              }
            });


            //dodatni podatki (stolpci), ki so vezani na celo vrstico
            //se ne prenašajo v bazo!
            metadata.additionalColumns.forEach(podatkiDodatnegaStolpca => {
              var cellValues = {} as CellValues;

              var podatkiTedna = podatkiDodatnegaStolpca.ColumnInfo.find(padavina => padavina.termin == teden.datumOd);
              if (podatkiTedna != undefined) {
                podatkiDodatnegaStolpca.CellPropertyDefinition.PropertyDefinitions.forEach(propDef => {
                  var key = propDef.Property as keyof PadavineResponseBase;
                  cellValues[propDef.Property] = podatkiTedna![key]?.toString();
                });
              }
              //preberemo in v vrstico zapisemo
              primaryRow.Values[podatkiDodatnegaStolpca.ColumnDataWrapper.Id] = new CellDataWrapper(
                data.id,
                inputData.DataType,
                {} as Meritev,
                podatkiDodatnegaStolpca.ColumnDataWrapper,
                podatkiDodatnegaStolpca.CellPropertyDefinition,
                cellValues,
                [undefined, undefined, undefined],
                true,
                //za test vzamemo kar to
                true
              )
            });
            vseVrsticeTedna.PrimaryRow = primaryRow;
            return vseVrsticeTedna;
          });


        //vrnemo transformirane in zbrane podatke zapakirane v tabeli prijazno obliko
        return new TableDataWrapper(rows, cellProperties, "TEZKE KOVINE - DEPOZICIJE", variabilniStolpci, staticniStolpci, this.inputData.TableMode == TableMode.Import ? 'PRENOS PODATKOV V BAZO' : null, metadata.additionalColumns.map(addC => addC.ColumnDataWrapper));
        break;

      //note: za analizo in koncentracijo bosta verjetno potrebni razlicni transformaciji
      case TableInputDataType.PMDelci:
        //variabilni stolpci so postaje
        var pmDelciInputData = inputData.Data as PMDelciResponse;
        var variabilniStolpci = pmDelciInputData.podatkiPostaj.map<ColumnDataWrapper>(postaja => {
          if (postaja == undefined)
            return new ColumnDataWrapper(`Neznana postaja ${postaja}`, "neznana postaja");
          else
            return new ColumnDataWrapper(postaja.postajaIme, postaja.id);
        });
        //nastavimo staticne stolpce
        var staticniStolpci = [
          new ColumnDataWrapper('Pozicija'),
          new ColumnDataWrapper('Izbranost'),
        ];

        //definiramo glavne podatke ( property-je celic).
        //definiramo tudi podatke, ki so samo za prikaz
        //prva je primarna
        var primarnaVrednost = new CellPropertyDefinition('koncentracija', 'Koncentracija')
        var cellProperties: CellPropertyDefinitionCollection = new CellPropertyDefinitionCollection(
          [
            primarnaVrednost,
            new CellPropertyDefinition('masa', 'Masa'),
            new CellPropertyDefinition('volumen_pretoka', 'Volumen pretoka')
          ],//vse celice
          primarnaVrednost,//primarna celica
          [
            new CellPropertyDefinition('sp_meja_pretoka', 'Spodnja meja pretoka'),
            new CellPropertyDefinition('veljavnost_kal', 'Veljavnost KAL')
          ],//samo za prikaz
          [
            new CellPropertyDefinition('komentar', 'komentar SKZ')
          ] //special
        );

        //vrstice so meritve posameznega dne
        //mesec, ki smo si ga izbrali, pride v metadata
        var mesecIndex = inputData.Metadata.mesec - 1;
        var leto = inputData.Metadata.leto;
        //izberemo vsak dan v mesecu. V obliki, ki je primerna za podatke, pridobljene iz api-ja
        // Create a new Date object for the given year and month
        const date = new Date(leto, mesecIndex, 1);
        // Get the number of days in the month
        const numOfDaysInMonth = new Date(leto, mesecIndex + 1, 0).getDate();
        const daysInMonth = Array.from({ length: numOfDaysInMonth }, () => {
          var newDate = new Date(date);
          date.setDate(date.getDate() + 1);
          return newDate;
        });
        //parameter bi moral za koncentracijo biti samo en:
        var pmDelciParametr = inputData.Parametri[0];
        var allEmepData = await this.dataRepository.GetEmepData(inputData.EmepEndpoint, inputData.AdditionalEmepInfo);

        //v podatke vrstice primarne vrstice vpišemo vrednosti za vsak stolpec posebej
        //kreirati moramo torej vrstice. Sli bomo cez vrstice in stolpce in pri vsaki vrstici za vsak stolpec nafilali primerne podatke.
        //vrstica so dnevi, stolpci so postaje. Primerni podatki so:
        //  podatki postaje, ki je v stolpcu, za dan, ki je v vrstici
        //tako dobimo celo tabelo
        var rows = daysInMonth.map<RowDataWrapper>(
          dateOfMonth => {
            //formatiramo datum, da je v takem formatu, kot termini v apiju
            //var termin = formatDate(dateOfMonth,'yyyy-MM-ddT00:00:00','en-SI');
            var vseVrsticeDneva: RowDataWrapper = new RowDataWrapper(cellProperties, new SimpleDisplayDataWrapper(formatDate(dateOfMonth, 'yyyy-MM-dd', 'en-SI'), formatDate(dateOfMonth, 'yyyy-MM-dd', 'en-SI')), variabilniStolpci, staticniStolpci);
            //primarna vrstica
            var primaryRow = new SingleRowDataWrapper(
              vseVrsticeDneva,     //gruča vseh vrstic h kateri spada ta
              dateOfMonth.getDate().toString(),               //index vrstice
              'PM delci'         //vrsta podatka
            );


            variabilniStolpci.forEach(async postajaStolpca => {
              //vsak stolpec je postaja
              //definirati moramo njegove podatke. Dobimo jih iz data (podatki endpoint). Torej pridobiti moramo meritev za vrstico (dan v mesecu)
              var meritevVrsticeZavariabilniStolpec = pmDelciInputData.podatkiPostaj?.find(postaja => //med podatki najdemo postajo,
                postaja.id.toString() == postajaStolpca.Id //ki ima ID, ki je definiran v stolpcu
                && postaja.podatki.find(podatek => podatek.parameterID == pmDelciParametr) != null //in ima podatke za parameter, ki ga iscemo (parameter od koncentracije)
                && postaja.podatki.find(podatek => podatek.parameterID == pmDelciParametr)?.meritve?.find(meritev => meritev?.termin != null && isSameDay(new Date(meritev.termin), dateOfMonth)) != null) //in ima med temi podatki zapis o dnevu, ki je v tej vrstici
                //ta postaja (stolpec) torej ima definirano meritev za ta dan (vrstico). To je meritev iz katere bomo črpali podatke, zato jo shranimo v spremenljivko
                ?.podatki?.find(podatek => podatek.parameterID == pmDelciParametr)?.meritve?.find(meritev => meritev?.termin != null && isSameDay(new Date(meritev.termin), dateOfMonth));
                //console.log({ps: postajaStolpca, data: pmDelciInputData});

              if (meritevVrsticeZavariabilniStolpec != null) {
                //glede na definirane glavne podatke celice in podatke, ki so samo za izpis, generiramo dinamičen class, ki drži
                //te nam pomembne vrednosti
                var cellValues = {} as CellValues;
                //v vrednosti celice prepišemo nam pomembne glavne podatke iz meritev
                cellProperties.PropertyDefinitions.forEach(propDef => {
                  var key = propDef.Property as keyof PMDelciMeritev;
                  cellValues[propDef.Property] = meritevVrsticeZavariabilniStolpec![key]?.toString();
                });
                //v vrednosti celice prepisemo nam pomembne podatke za izpis iz meritev
                cellProperties.DisplayPropertyDefinitions.forEach(displayPropDef => {
                  var key = displayPropDef.Property as keyof PMDelciMeritev;
                  cellValues[displayPropDef.Property] = meritevVrsticeZavariabilniStolpec![key]?.toString();
                });
                //v vrednosti celice prepisemo posebne podatke
                cellProperties.SpecialPropertyDefinitions.forEach(specPropdef => {
                  var key = specPropdef.Property as keyof PMDelciMeritev;
                  cellValues[specPropdef.Property] = meritevVrsticeZavariabilniStolpec![key]?.toString();
                });
                //console.log({meritev: meritevVrsticeZavariabilniStolpec, cell: cellValues});
                //NASTAVIMO EMEP DATA
                //console.log(meritevVrsticeZavariabilniStolpec);
                var emepData : EMEPData[] | undefined[] = [undefined,undefined,undefined];
                if(meritevVrsticeZavariabilniStolpec?.emep != null){
                  if(!Array.isArray(meritevVrsticeZavariabilniStolpec?.emep) && meritevVrsticeZavariabilniStolpec.emep != null){
                    meritevVrsticeZavariabilniStolpec.emep = (meritevVrsticeZavariabilniStolpec.emep as string)?.split(',');
                  }
                  emepData = meritevVrsticeZavariabilniStolpec?.emep?.map<number>(emepZastavica => Number.parseInt(emepZastavica))?.map<EMEPData>((number) => {
                    var zastavica = allEmepData.find(emepData => emepData.ID == number)!
                    if (zastavica == null) {
                      console.error(`Zastavica z ID ${number} ni definirana!`);
                      //TEMP FIX
                      zastavica = EMEPData.Unknown;
                    }
                    return zastavica!;
                  });
                }
                //console.log(emepData);

                var initialEmepData : [EMEPData | undefined, EMEPData | undefined, EMEPData | undefined]= [emepData[0], emepData[1], emepData[2]]
                //KAl -> ce nam kal sporoci, da je podatek neveljaven, mu damo zastavico. Ta zastavica gre tudi v bazo ob vnosu
                if (inputData.TableMode == TableMode.Import) {
                  if (cellValues['veljavnost_kal'] != null && Number.parseInt(cellValues['veljavnost_kal']) == -1) {
                    console.debug(`veljavnost_kal je -1!  (postaja ${postajaStolpca.Id}; dan ${dateOfMonth} )`);
                    initialEmepData[0] = new EMEPData("Neveljavnost KAL", true, 99);
                  }
                }

                //preberemo in v vrstico zapisemo podatke o depoziciji
                primaryRow.Values[postajaStolpca.Id!] = new CellDataWrapper(
                  formatDate(dateOfMonth, 'yyyy-MM-dd', 'en-SI'),
                  inputData.DataType,
                  meritevVrsticeZavariabilniStolpec,
                  postajaStolpca,
                  cellProperties,
                  cellValues,
                  initialEmepData,
                  meritevVrsticeZavariabilniStolpec.veljavnost_skz == 1,
                  //za test vzamemo kar to
                  false
                )


              } else {
                //podatkov za stolpec nismo nasli
                if (meritevVrsticeZavariabilniStolpec == null)
                  console.warn(`Podatki postaje ${postajaStolpca.Id} za termin ${dateOfMonth} so prazni!`);
              }
            });
            //to, da imamo vec k samo primary vrstico, je depricated
            vseVrsticeDneva.PrimaryRow = primaryRow;
            return vseVrsticeDneva;
          });
        return new TableDataWrapper(rows, cellProperties, "PM DELCI", variabilniStolpci, staticniStolpci, this.inputData.TableMode == TableMode.Import ? 'PRENOS PODATKOV V BAZO' : null);
        break;
      default:
        throw new Error('Cannot transform rawData');
    }
  }

  //racunanje podatkov, ki ne pridejo iz backenda
  private izracunDepozicije(
    volumentPadavine: number,
    koncentracijaPadavine: number,
    volumenSuhUsed: number,
    koncentracijaSuhUsed: number,
    premerLijaka: number = 1
  ) {
    var povrsinaLijaka = (premerLijaka / 2) * (premerLijaka / 2) * Math.PI;
    //depozicija = ((volumen padavine(ml) * koncentracija padavine(ug/ml)) + (volumen suhe used.(ml) * koncentracija suhe used(ug/ml)))/ (1000* površina lijaka)
    return ((volumentPadavine * koncentracijaPadavine) + (volumenSuhUsed * koncentracijaSuhUsed)) / (1000 * povrsinaLijaka);
  }

  //prikazovanje grafa
  public ShowGraphClick(col: ColumnDataWrapper) {
    const graphModal = this.modalService.open(GraphModalContentComponent, { size: 'xl' });
    graphModal.componentInstance.GraphsData = this.tableData.AllCellDataOfColumnAsGraph(col, this.tableData.PropertyDefinitions);
    graphModal.result.then(
      (dismissed) => { console.log("Dismissed modal: " + dismissed) },
      (reason) => {
        console.log(`Dismissed cell ${this.getDismissReason(reason)}`);
      }
    );
  }

  //odpiranje modala za posiljanje v novo bazo
  public SendDataToDBClick(button: HTMLElement) {
    //nastavimo gumb na unclickable
    var initialText = button.innerText;
    button.innerText = "Pošiljanje podatkov...";
    button.setAttribute('disabled', 'true');

    const cellModal = this.modalService.open(SelectRowsModalComponent, { size: 'md' });
    cellModal.componentInstance.Title = "Izvoz podatkov v novo bazo";
    cellModal.componentInstance.NumberOfRows = this.tableData.NonEmptyPrimaryRows.length;
    cellModal.componentInstance.NumberOfSelectedRows = this.tableData.NonEmptyPrimaryRows.filter(row => row.Selected == true).length;
    cellModal.result.then(
      (onlySelectedRows) => {
        this.SendDataToDB(onlySelectedRows, button, initialText);
      },
      (reason) => {
        console.log(`Dismissed modal with reason: ${this.getDismissReason(reason)}`);
        //nastavimo gumb nazaj na clickable
        button.innerText = initialText;
        button.removeAttribute('disabled');
      }
    );
  }


  //posiljanje podatkov v naso bazo
  public SendDataToDB(onlySelectedRows: boolean, button: HTMLElement, buttonTextOnFinish: string) {


    //izberemo vse podatke
    var unsentCells: CellDataWrapper[] = [];
    if (!onlySelectedRows)
      unsentCells = this.allCells.filter(cell => cell.ZePreneseniVBazo == false);
    else {
      var selectedRows = this.tableData.PrimaryRows.filter(row => row.Selected == true);
      selectedRows.forEach(row => unsentCells = [...unsentCells, ...row.GetAllCells().filter(cell => cell.ZePreneseniVBazo == false)])
      //debug
      console.log(`selected rows: ${selectedRows.length}, sent cells: ${unsentCells.length}`);
    }
    var importCellsRequest: InsertMeritev[] = [];
    unsentCells.forEach(cell => importCellsRequest.push(cell.ToInsertMeritev()));
    this.dataService.insertData(importCellsRequest).subscribe(data => {
      console.log({importingCells: importCellsRequest, returnedCells: data});
      //oznacimo celice, ki so se uspesno uvozile
      var successCells = this.tableData.GetIdentifiedCells(data.filter(meritev => meritev.success == true));
      successCells.forEach(cell => cell.ZePreneseniVBazo = true);
      //pridobimo vse celice, ki smo jih poskusali poslati
      var sentCells = this.tableData.GetIdentifiedCells(data);
      //blinkamo celice glede na to ali so bile uspesno uvozene ali ne
      this.subsequentlyBlinkCells(sentCells, 3000, (cell) => cell.ZePreneseniVBazo ? "#198754" : "#FF0000", 0, 50);
      console.log(`USPESNO UVOZENIH ${successCells.length} celic. \n NEUSPESNO UVOZENIH ${data.length - successCells.length}`);
      //nastavimo gumb nazaj na clickable
      button.innerText = buttonTextOnFinish;
      button.removeAttribute('disabled');
    });



  }

  private subsequentlyBlinkCells(cells: CellDataWrapper[], blinkTime: number, blinkColorLogic: (cell: CellDataWrapper) => string, initialDelay: number, delayBetweenCellBlinks: number) {
    var _initialDelay = initialDelay;
    cells.forEach(cell => {
      cell.Blink(blinkColorLogic(cell), blinkTime, initialDelay);
      _initialDelay += delayBetweenCellBlinks;
    });
  }

  public UpdateCellsInDBClick(button: HTMLElement) {
    var initialText = button.innerText;
    button.innerText = "Pošiljanje podatkov...";
    button.setAttribute('disabled', 'true');
    const cellModal = this.modalService.open(SelectRowsModalComponent, { size: 'md' });
    cellModal.componentInstance.Title = "Posodabljanje podatkov";
    cellModal.componentInstance.NumberOfRows = this.tableData.NonEmptyPrimaryRows.length;
    cellModal.componentInstance.NumberOfSelectedRows = this.tableData.NonEmptyPrimaryRows.filter(row => row.Selected == true).length;
    cellModal.result.then(
      (onlySelectedRows) => {
        this.UpdateCellsInDB(onlySelectedRows, button, initialText);
      },
      (reason) => {
        console.log(`Dismissed modal with reason: ${this.getDismissReason(reason)}`);
        //nastavimo gumb nazaj na clickable
        button.innerText = initialText;
        button.removeAttribute('disabled');
      }
    );

  }
  //vse podatke, ki so prikazani v tabeli, posodobimo
  public UpdateCellsInDB(onlySelectedRows: boolean, button: HTMLElement, buttonTextOnFinish: string) {

    //posodobimo vse celice (kasneje async)
    var cellsToUpdate: CellDataWrapper[] = [];
    if (!onlySelectedRows)
      cellsToUpdate = this.tableData.allCells.filter(cell => cell.Status == Status.Spremenjeno);
    else {
      var selectedRows = this.tableData.PrimaryRows.filter(row => row.Selected == true);
      selectedRows.forEach(row => cellsToUpdate = [...cellsToUpdate, ...row.GetAllCells().filter(cell => cell.Status == Status.Spremenjeno)])
    }



    console.log("Spremenjenih celic: " + cellsToUpdate.length);
    var meritveForUpdate: UpdateMeritev[] = [];
    cellsToUpdate.forEach(cell => {console.log(cell.ToUpdateMeritev());meritveForUpdate.push(cell.ToUpdateMeritev())});
    this.dataService.UpdateCellsInDB(meritveForUpdate).subscribe(data => {
      console.log({cellsToUpdate: cellsToUpdate, returnedCells: data});
      //oznacimo celice, ki so se uspesno uvozile
      var successCells = this.tableData.GetIdentifiedCells(data.filter(meritev => meritev.success == true));
      successCells.forEach(cell => cell.ZePreneseniVBazo = true);
      //pridobimo vse celice, ki smo jih poskusali poslati
      var sentCells = this.tableData.GetIdentifiedCells(data);
      //blinkamo celice glede na to ali so bile uspesno uvozene ali ne
      this.subsequentlyBlinkCells(sentCells, 3000, (cell) => cell.ZePreneseniVBazo ? "#198754" : "#FF0000", 0, 50);
      console.log(`USPESNO UVOZENIH ${successCells.length} celic. \n NEUSPESNO UVOZENIH ${data.length - successCells.length}`);
      //nastavimo gumb nazaj na clickable
      button.innerText = buttonTextOnFinish;
      button.removeAttribute('disabled');
    });


    //console.log("Update meritve request " + JSON.stringify(request, null, 2));
    //TODO dodaj klic na backend, kakšen naj bo klic in kakšen bo response
    //DEBUG
    //onend
    button.innerText = buttonTextOnFinish;
    button.removeAttribute('disabled');
  }

  //ali pokazemo gumb za posiljanje podatkov v naso bazo
  public ShowSendDataToDBButton(): boolean {
    return this.inputData.TableMode == TableMode.Import;
  }

}

//primerjava dneva (uro/minute) zanemarimo
function isSameDay(date1: Date, date2: Date): boolean {
  return (
    date1.getFullYear() === date2.getFullYear() &&
    date1.getMonth() === date2.getMonth() &&
    date1.getDate() === date2.getDate()
  );
}

export interface ITableDataInput {
  DataType: TableInputDataType;
  Parametri: number[];
  Data: ResponseBase;
  AdditionalEmepInfo: AdditionalEmepInfo[];
  EmepEndpoint: string;
  TableMode: TableMode;
  Metadata?: any; //razni podatki, ki jih posljemo v tabelo. Obicajno so to podatki, ki jih tabela potrebuje za transformacijo
}
export enum TableMode {
  Edit,
  EditOnlyEMEPAndValidity,
  Import
}
export interface AdditionalEmepInfo {
  ID: number;
  ForceStanje: Stanje;

}
export enum TableInputDataType {
  TezkeKovine = "tedenske_pad_kovine", //ko kreiramo insertRequest, je to podatek o tipu meritve
  PMDelci = "pm"
}




