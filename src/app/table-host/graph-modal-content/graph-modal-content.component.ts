import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ChartConfiguration, ChartData, ChartEvent, ChartOptions, ChartType } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';
import DataLabelsPlugin from 'chartjs-plugin-datalabels';
import { CellDataWrapper, CellPropertyDefinition, CellPropertyDefinitionCollection, CellValues } from '../tableClasses';

//vsi podatki grafa (vbistvu vsebuje vec grafov za dolocen parameter. Vsak graf predstavlja eno vrednost v GraphDatas)
export class FullGraphData {
  constructor(public GraphData: GraphDataWrapper[]) { }

}
//vsi podatki za generiranje enega grafa
export class GraphDataWrapper {
  ValueProperties!: CellPropertyDefinitionCollection;
  ParameterName!: string;
  VrstaPadavineName!: string;
  Data: GraphUnit[] = [];

  //vrne 'oznake' v grafu. To so imena GraphUnit-ov.  Primer: tedni
  public get Labels(): string[] {
    return this.Data.map<string>(data => data.Name);
  }
  //vrne vrednosti v grafu
  public get Values(): CellValues[] {
    return this.Data.map<CellValues>(data => data.Value);
  }
  public GetValuesOfProperty(property: keyof CellValues, fixedLength: number | null = null): number[] {
    return this.Data.map<number>(data => {
      var val = data?.Value != null && data.Value[property] != null ? Number.parseFloat(data.Value[property]!) : 0;
      if (fixedLength == null)
        return val;
      else
        return Number.parseFloat(val.toFixed(fixedLength));
    })
  }


}

//enote na grafu
export class GraphUnit {
  constructor(public Name: string, public Value: CellValues) { }
}



@Component({
  selector: 'app-graph-modal-content',
  templateUrl: './graph-modal-content.component.html',
  styleUrls: ['./graph-modal-content.component.css']
})
export class GraphModalContentComponent implements OnInit {

  //vsi podatki o grafih, ki pridejo iz tabele
  @Input()
  public GraphsData!: FullGraphData;

  //depricated
  public CurrentlyDisplayedGraph!: GraphDataWrapper;
  //trenutno prikazani propertyji
  public CurrentlyDisplayedProperties: CellPropertyDefinition[] = [];
  //podatki grafa
  public barChartData!: ChartData<'bar'>;
  //fiksna dolžina stevil v grafu
  public fixedValLength : number | null =3;

  //naslov modala
  ModalName!: string;
  //definirani property-ji vrednosti
  ValueProperties!: CellPropertyDefinitionCollection;

  constructor(
    public modalService: NgbModal,
    public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
    this.ModalName = this.GraphsData.GraphData[0].ParameterName;
    this.ValueProperties = this.GraphsData.GraphData[0].ValueProperties;
    this.CurrentlyDisplayedGraph = this.GraphsData.GraphData[0];
    this.DisplayInitialGraph();
  }

  //inicialni graf (prikaze primarny property)
  DisplayInitialGraph() {
    this.CurrentlyDisplayedProperties.push(this.ValueProperties.PrimaryPropertyDefinition);
    this.barChartData = {
      labels: this.CurrentlyDisplayedGraph.Labels,
      datasets: [
        { data: this.CurrentlyDisplayedGraph.GetValuesOfProperty(this.ValueProperties.PrimaryPropertyDefinition.Property,this.fixedValLength), label: this.ValueProperties.PrimaryPropertyDefinition.PropertyDisplayName, yAxisID: 'yLeft' }
      ]
    };
  }

  //grafu dodamo/odvzamemo izris za property
  DisplayPropertyClick(property: CellPropertyDefinition, leftYAxis: boolean) {
    if (this.IsPropertyVisualized(property))
      this.removeDisplayProperty(property);
    else
      this.displayProperty(property, leftYAxis);

  }

  //v grafu odstranimo izris za property
  private removeDisplayProperty(property: CellPropertyDefinition) {
    this.CurrentlyDisplayedProperties = this.CurrentlyDisplayedProperties.filter(prop => prop != property);
    this.barChartData.datasets = this.barChartData.datasets.filter(dataset => dataset.label != property.PropertyDisplayName);
    this.chart.update();
  }
  //v graf dodamo izris za property
  private displayProperty(property: CellPropertyDefinition, leftYAxis: boolean) {
    var propertyValues = this.GraphsData.GraphData[0].GetValuesOfProperty(property.Property,this.fixedValLength);
    this.CurrentlyDisplayedProperties.push(property);
    //dodamo podatke v graf
    this.barChartData.datasets.push({ data: propertyValues, label: property.PropertyDisplayName, yAxisID: leftYAxis ? 'yLeft' : 'yRight' });
    this.chart.update();
  }

  //ali trenutno vizualiziramo graf
  public IsPropertyVisualized(property: CellPropertyDefinition) {
    return this.CurrentlyDisplayedProperties.includes(property);
  }

  //GRAF
  @ViewChild(BaseChartDirective) chart!: BaseChartDirective;

  //nastavitve grafa
  public barChartOptions: ChartConfiguration['options'] = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      yLeft: {
        type: 'linear',
        display: true,
        position: 'left',
      },
      yRight: {
        type: 'linear',
        display: true,
        position: 'right',
      },
    },
    plugins: {
      legend: {
        display: true,
      },
      datalabels: {
        display: true
      }
    }
  };
  public barChartType: ChartType = 'bar';
  public barChartPlugins = [
    DataLabelsPlugin
  ];




  // events
  public chartClicked({ event, active }: { event?: ChartEvent, active?: {}[] }): void {
    return;
  }

  public chartHovered({ event, active }: { event?: ChartEvent, active?: {}[] }): void {
    return;
    console.log(event, active);
  }




}





