import { Meritev } from "../models/Meritev";
import { InsertMeritev, InsertMeritevRequest } from "../models/Requests/InsertTezkeKovineRequest";
import { UpdateMeritev } from "../models/Requests/UpdateMeritveRequest";
import { MeritevResponseBase } from "../models/Responses/MeritveResponse";
import { TezkeKovineMeritev } from "../models/Responses/TezkeKovineResponse";
import { EMEPData } from "../services/data.service";
import { FullGraphData, GraphDataWrapper, GraphUnit } from "./graph-modal-content/graph-modal-content.component";
import { TableInputDataType } from "./table-host.component";

export enum Status {
    Nespremenjeno = 'Nespremenjeno',
    Spremenjeno = 'Spremenjeno',
    Shranjeno = 'Shranjeno'
}
export enum Stanje {
    Rdece = '#c42929;  text-decoration: line-through; ',
    Oranzno = '#f28a3f',
    Zeleno = '#abd9b5',
    Vijolicno = '#895ba6',
    Brez = 'white',

}
//podatki, ki definirajo vrstico
export class ColumnDataWrapper {
    public static StaticColumnOrUndefinedKeyPrefix = "KEY_OF_COLUMN_";
    private id?: keyof RowValues;
    get Id(): (keyof RowValues & string) | string {
        //console.log(this.DisplayName + "  ID: " +this.id);
        if (this.id == undefined)
            return ColumnDataWrapper.StaticColumnOrUndefinedKeyPrefix + this.DisplayName;
        else
            return this.id.toString();
    }
    DisplayName!: string;
    get ShortDisplayName() {
        var shortName = this.DisplayName;
        //ce ima dva presledka, potem pri drugem porezemo. Drugace ne, da ne unicimo imena krajev kot so npr. novo mesto
        const count = (shortName.match(new RegExp(' ', "g")) || []).length;
        if (count > 1) {
            shortName = shortName.substring(0, shortName.indexOf(' ', shortName.indexOf(' ') + 1));
            //vcasih imamo ime stolpca npr. 'Kalij v vzorcu'. v takih primerih se to pokaže kot 'Kalij v'. Tazadnji ' v' nam nič ne pomeni, zato ga odrežemo
            if (shortName.endsWith(' v'))
                shortName = shortName.substring(0, shortName.lastIndexOf(' v'));
        }
        //ce imamo ID, dodamo se to (v oklepaj)
        if (this.id != null)
            shortName = `${shortName} (${this.id})`;

        //ce je se vedno prekratko, ga skrajsamo in dodamo pike
        if (shortName.length > 15)
            shortName = `${shortName.substring(0, 15)}..`;

        return shortName;
    }
    get FullDisplayName() {
        if (this.id != null)
            return `${this.DisplayName} (${this.id})`;
        else
            return this.DisplayName;
    }
    constructor(name: string, id: keyof RowValues | undefined = undefined) {
        this.DisplayName = name;
        this.id = id;
    }
}




//objekt, ki vsebuje vse potrebne informacije za kreiranje tabele.
//konstruira se na podlagi podatkov, ki jih dobimo iz backenda.
export class TableDataWrapper {
    //primarne vrstice za izpis v tabeli
    get PrimaryRows() {
        return this.AllRows.map<SingleRowDataWrapper>(row => row.PrimaryRow);
    }
    //pridobi primarne vrstice, ki imajo vsaj eno vrednost
    get NonEmptyPrimaryRows() {
        return this.PrimaryRows.filter(row => row.HasNoData == false);
    }
    //pridobi primarne vrstice, ki imajo vsaj eno vrednost, ali pa imajo njene pripadajoce sekundarne vrstice vsaj eno vrednost
    get NonCompletelyEmptyRows() {
        return this.AllRows.filter(rowWrapper => {
            var retRow = false;
            //ce ima primarna vrstica vrdnosti, potem vrnemo vrstico
            if (rowWrapper.PrimaryRow.HasNoData == false)
                return true;
            //ce ima katera izmed sekundarnih vrstic vrednosti, potem vrnemo vrstico
            rowWrapper.SecondaryRows?.forEach(
                rowData => {
                    if (rowData.HasNoData == false)
                        retRow = true;
                });
            if (retRow)
                return true;
            else
                return false;
        })
            //od takih vrstic vrnemo samo primarne vrstice
            .map<SingleRowDataWrapper>(row => row.PrimaryRow);

    }
    ///stolpci
    //vsi stolpci
    get allColumns() { return [...this.StaticColumns, ...(this.AdditionalColumns ?? []), ...this.VariableColumns] }
    //dobimo stolpce, ki držijo vrednost te vrstice. Niso vključeni common(additional) stolpci)
    get dataColumns() { return this.VariableColumns }

    //vse celice
    get allCells() {
        var cells: CellDataWrapper[] = [];
        this.AllRows.forEach(row => cells = [...cells, ...row.PrimaryRow.GetAllCells()]);
        return cells;
    }

    constructor(public AllRows: RowDataWrapper[], public PropertyDefinitions: CellPropertyDefinitionCollection, public Name: string, public VariableColumns: ColumnDataWrapper[], public StaticColumns: ColumnDataWrapper[], public Subname: string | null = null, public AdditionalColumns: ColumnDataWrapper[] | null = null) {
    }

    //pridobi vse podatke določenega stolpca (parametra)
    public AllCellDataOfColumnAsGraph(column: ColumnDataWrapper, properties: CellPropertyDefinitionCollection): FullGraphData {
        var primaryRowGraphData: GraphDataWrapper = new GraphDataWrapper();
        //nastavimo ime parametra, ki ga vizualiziramo
        primaryRowGraphData.ParameterName = column.FullDisplayName;
        //nastavimo vrsto padavine, ki jo trenutno vizualiziramo
        primaryRowGraphData.VrstaPadavineName = this.Name;
        //nastavimo vrednosti propertyjev
        primaryRowGraphData.ValueProperties = properties;
        //todo dodaj se podatke ostalih vrst padavin, za lahko preklapljanje v vizualizaciji
        var secundaryRowGraphData: GraphDataWrapper[] = [];
        this.AllRows.forEach(
            rowDataWrapper => {
                //poberemo podatke primarnih vrstic
                if (!rowDataWrapper.PrimaryRow.HasNoData)
                    primaryRowGraphData.Data.push(new GraphUnit(rowDataWrapper.PrimaryRow.DisplayName, rowDataWrapper.PrimaryRow.getVariableColumnCellData(column)?.Value));
            });

        return new FullGraphData([primaryRowGraphData]);
    }


    //pridobi vse celice, ki jih opisuje vhodni parameter
    GetIdentifiedCells(meritveData: MeritevResponseBase[]) {
        var foundCells: CellDataWrapper[] = [];
        var allCells = this.allCells;
        //gremo cez vse meritve
        meritveData.forEach(meritev => {
            //najdemo celico, ki ima vse podatke enake kot vhodna meritev
            var cellForMeritev = allCells.find(
                cell => cell.DerivesFromMeritev(meritev)
            );
            //ce celica obstaja jo zapisemo v returned
            if (cellForMeritev != undefined)
                foundCells.push(cellForMeritev);
            else
                console.error(`Ne najdem celice, ki bi ustrezala meritvi ${JSON.stringify(meritev, null, 2)}`);
        })
        return foundCells;
    }

}


export interface DisplayDataWrapper {
    get ShortDisplayName(): string;
    get DisplayName(): string;
}
export class SimpleDisplayDataWrapper implements DisplayDataWrapper {
    constructor(private displayName: string, private shortName: string) { }
    get ShortDisplayName(): string {
        return this.displayName;
    }
    get DisplayName(): string {
        return this.shortName;
    }

}

export class DisplayTedenDataWrapper implements DisplayDataWrapper {
    get ShortDisplayName() {
        return `${this.DisplayZacetek} (${this.TedenIndex})`;
    }
    get DisplayName() {
        return `Teden ${this.TedenIndex}: ${this.DisplayZacetek} - ${this.DisplayKonec}`;
    }
    private get DisplayZacetek() {
        return `${new Date(this.Zacetek).toLocaleDateString()}`;
    }
    private get DisplayKonec() {
        return `${new Date(this.Konec).toLocaleDateString()}`;
    }
    constructor(public TedenIndex: string, public Zacetek: Date, public Konec: Date) { }
}

//vsi podatki in logika obnašanja posamezne vrstice
export class RowDataWrapper {
    PrimaryRow!: SingleRowDataWrapper;
    SecondaryRows?: SingleRowDataWrapper[];
    //kratek naziv vrstice, ki se pokaže v tabeli
    get DisplayName() {
        return this.RowName.ShortDisplayName;
    }
    constructor(public CellPropertyDefinitions: CellPropertyDefinitionCollection, public RowName: DisplayDataWrapper, public VariableColumns: ColumnDataWrapper[], public StaticColumns: ColumnDataWrapper[], public AdditionalColumns: ColumnDataWrapper[] | null = null) { }
    public StartChangingCellData() {
        this.PrimaryRow.StartChangingCellData();
        if (this.SecondaryRows != null)
            this.SecondaryRows.forEach(row => row.StartChangingCellData())
    }
}
//vsi podatki posamezne vrstice - tudi podatki ostalih meritev, ki se prikažejo ob kliku na vrstico
export class SingleRowDataWrapper {
    Name!: DisplayDataWrapper;
    Stanje: Stanje;
    Status: Status;
    Values!: RowValues;
    Selected: boolean = false;

    //krajse ime za prikaz
    get DisplayName() {
        return this.Name.ShortDisplayName;
    }

    //ali je v vrstici sploh kaj podatkov
    get HasNoData(): boolean {
        //ce nima nic vrednosti, potem je prazna
        if (this.Values == null || Object.keys(this.Values)?.length == 0)
            return true;
        //ce nima nic vrednosti iz stolpcev, ki predstavljajo ne-skupne vrednosti celice, potem vrnemo, da je prazna
        var numOfAddColumnData = 0;
        //preverimo kolk ne-skupnih vrednosti ima izpolnjenih. Ce so to vse vrednosti, ki ima izpolnjene, potem je vrstica prazna
        this.ClusterRowData.AdditionalColumns?.forEach(col => {if(this.getVariableColumnCellData(col) != null){numOfAddColumnData++;} });
        if (Object.keys(this.Values).length == numOfAddColumnData)
            return true;

        return false;
    }
    constructor(public ClusterRowData: RowDataWrapper, public Position: string, public DataType: string) {
        this.Stanje = Stanje.Brez;
        this.Status = Status.Nespremenjeno;
        this.Name = ClusterRowData.RowName;
        this.Values = new RowValues();
    }
    //pridobimo osnovne podatke vrstice (te, ki pridejo iz statičnih stolpcev).
    //vsi stolpci so zapisani v TableDataWrapper.staticColumns
    public getBaseColumnData(baseColumnID: string) {
        switch (baseColumnID) {
            case ColumnDataWrapper.StaticColumnOrUndefinedKeyPrefix + "Pozicija":
                return this.Position?.toString();
            case ColumnDataWrapper.StaticColumnOrUndefinedKeyPrefix + "Stanje":
                return this.Stanje.toString();
            case ColumnDataWrapper.StaticColumnOrUndefinedKeyPrefix + "Status":
                return this.Status.toString();
            default:
                return "";
        }
    }
    //pridobivanje vrednosti posamezne celice v vrstici glede na podan stolpec
    public getVariableColumnCellData(variableColumn: ColumnDataWrapper): CellDataWrapper {
        return this.Values[variableColumn.Id];
    }

    //spremenimo podatke v celici in gremo v v status "spremenjeno", ce je tudi celica v tem stanju
    public UpdateCellAllData(cellData: CellDataWrapper) {
        //spremenimo vrednost celice
        console.log(cellData);
        var cellChanged = cellData.CommitChanges();
        if (cellChanged)
            this.Status = Status.Spremenjeno;
    }
    //spremenimo samo emep zastavice  v celici in gremo v v status "spremenjeno", ce je tudi celica v tem stanju
    public UpdateCellEMEPData(cellData: CellDataWrapper) {
        //spremenimo vrednost celice
        var cellChanged = cellData.CommitChangedEmepData();
        if (cellChanged)
            this.Status = Status.Spremenjeno;
    }
    //vse celice pripravimo za urejanje
    public StartChangingCellData() {
        Object.keys(this.Values).forEach(propertyKey => this.Values[propertyKey]?.StartChangingCell());
    }

    //pridobimo vse celice
    public GetAllCells(): CellDataWrapper[] {
        var cells: CellDataWrapper[] = [];
        Object.keys(this.Values).forEach(propertyKey => cells.push(this.Values[propertyKey]));
        return cells;
    }
    public GetChangableCells(): CellDataWrapper[] {
        var cells: CellDataWrapper[] = [];
        Object.keys(this.Values).forEach(propertyKey => propertyKey!= null && !propertyKey.startsWith(ColumnDataWrapper.StaticColumnOrUndefinedKeyPrefix) ? cells.push(this.Values[propertyKey]) : null);

        return cells;
    }

    public Select(bool: boolean) {
        this.Selected = bool;
    }


}
//surovi, dinamični podatki vrstice v tabeli
export class RowValues {
    [key: string]: CellDataWrapper
}
//vsi podatki neke celice
export class CellValues {
    [key: string]: string | undefined
}

//definiramo mozne property-je celice in njihov izpis
export class CellPropertyDefinition {
    constructor(public Property: keyof CellValues | string, public PropertyDisplayName: string) { }
}

//kolekcija CellPropertyDefinitions. Novi dodatne podatke
export class CellPropertyDefinitionCollection {
    constructor(
        public PropertyDefinitions: CellPropertyDefinition[],               //glavni podatki meritev -  property-ji neke celice
        public PrimaryPropertyDefinition: CellPropertyDefinition,          //definira kater izmed property-jev je "glavni" - se obravnava malo drugače
        public DisplayPropertyDefinitions: CellPropertyDefinition[] = [],   //podatki določene meritve, ki so samo za prkaz
        public SpecialPropertyDefinitions: CellPropertyDefinition[] = []   //posebni podatki - jih ne prikazujemo kot glavne ampak se jih da urejat (primer: opombe (komentar))
    ) { }
}
//* celice imajo lahko sedaj več vrednosti hkrati
//podatki posamezne celice
export class CellDataWrapper {


    ///VREDNOST CELICE
    //nova vrednost celice, kadar jo urejamo
    public ChangedValue?: CellValues | undefined;
    //prvotna vrednost, ki pride iz backenda
    readonly InitialValue!: CellValues;
    //trenutna vrednost celice
    protected _value: CellValues;
    public get Value(): CellValues {
        return this._value;
    }

    ///VELJAVNOST CELICE
    //prvotna
    readonly InitialVeljavnost: boolean;
    //nova
    private _changedVeljavnost: boolean;
    public get ChangedVeljavnost(): boolean {
        return !this.checkForForceInvalid(this.ChangedEmepData);
    }
    public set ChangedVeljavnost(value: boolean) {
        if (this.ChangedIsForcedInvalid && value == true) {
            console.error("Podatku ne moremo spremeniti veljavnosti, ker neveljavnost forsira zastavica");
            return;
        }
        this._changedVeljavnost = value;
    }
    //trenutna
    private _veljavnost: boolean;
    public get veljavnost(): boolean {
        return !this.checkForForceInvalid(this.EmepData);
    }

    //stanje celice
    Stanje: Stanje = Stanje.Brez;
    //status celice
    Status: Status = Status.Nespremenjeno;
    //utripanje celice
    public BlinkColor: string = "";
    public IsBlinking: boolean = false;
    private blinkTimer?: ReturnType<typeof setTimeout>;

    ///EMEP DATA
    //prvotna
    readonly IntialEmepData: [EMEPData, EMEPData, EMEPData];
    //trenutna
    private _emepData: [EMEPData, EMEPData, EMEPData];
    public get EmepData(): [EMEPData, EMEPData, EMEPData] {
        return this._emepData;
    }
    //nova
    private _ChangedEmepData!: [EMEPData, EMEPData, EMEPData];
    public get ChangedEmepData(): [EMEPData, EMEPData, EMEPData] {
        return this._ChangedEmepData;
    }
    public set ChangedEmepData(value: [EMEPData, EMEPData, EMEPData]) {
        //na nivoju celice morajo biti zastavice unikatne
        value = this.forceUniqueEmep(value);

        //preverimo, ce katera nova zastavica forsira neveljavnost podatka
        this.ChangedIsForcedInvalid = this.checkForForceInvalid(value);
        this._ChangedEmepData = value;
    }
    //ali ima celica ze zasedene vse prostore za EMEP zastavice
    public get HasFullChangedEmepData(): boolean {
        return this.ChangedEmepData != null && this.ChangedEmepData.filter(emep => emep.ID != null).length == 3;
    }
    ///FORSIRANJE NEVELJAVNOSTI CELICE
    //ali katera trenutna zastavica forsira neveljavnost trenutnega podatka
    private _IsForcedInvalid: boolean;
    public get IsForcedInvalid(): boolean {
        return this._IsForcedInvalid;
    }
    public set IsForcedInvalid(value: boolean) {
        if (value == true)
            this._veljavnost = false;
        this._IsForcedInvalid = value;
    }
    //ali katera trenutna zastavica forsira neveljavnost spremenjenega podatka
    private _changedIsForcedInvalid: boolean;
    public get ChangedIsForcedInvalid(): boolean {
        return this._changedIsForcedInvalid;
    }
    public set ChangedIsForcedInvalid(value: boolean) {
        if (value == true)
            this.ChangedVeljavnost = false;
        this._changedIsForcedInvalid = value;
    }

    get PrimaryValue(): string | undefined {
        return this.Value[this.PropertyDefinition.PrimaryPropertyDefinition.Property];
    }
    //vrednost, ki se prikaže
    get DisplayValue(): string | undefined {
        if (this.PrimaryValue != null && this.PrimaryValue.length > 5)
            return this.PrimaryValue.substring(0, 5);
        else
            return this.PrimaryValue;
        return "";
    }
    //id stolpca v katerem je celica
    get ColumnID() {
        return this.Column?.Id;
    }

    //dodatni podatki celice. Se pokažejo na hover
    private _popupData?: PopupData;
    get PopupData() {
        if (this._popupData == null) {
            this._popupData = new PopupData();
            this._popupData.PropertyDefinition = this.PropertyDefinition;
            this._popupData.Values = this.Value;
            this._popupData.EmepData = this._emepData;
            if (this._emepData == null || this._emepData == undefined)
                this.Status = Status.Spremenjeno;
            this._popupData.IsValid = this.veljavnost;
        }
        return this._popupData;
    }
    set PopupData(newPopupData: PopupData) {
        this._popupData = newPopupData;
    }
    //dodatni podatki celice. Se pokažejo na hover. Vsebujejo tudi trenutne, necommitane spremembe celice
    private _changePopupData?: PopupData;
    get ChangePopupData() {
        if (this._changePopupData == null) {
            this._changePopupData = new PopupData();
            //if (this.ChangedValue != null) {
            //  this._changePopupData.FullInitialPrimaryValue = this.ChangedValue[this.ValuePrimaryProperty];
            //}
            if (this.ChangedValue != null)
                this._changePopupData.Values = this.ChangedValue;

            this._changePopupData.EmepData = this._ChangedEmepData;
            this._changePopupData.IsValid = this.ChangedVeljavnost;
        }
        return this._changePopupData;
    }
    set ChangePopupData(newPopupData: PopupData) {
        this._popupData = newPopupData;
    }
    constructor(
        readonly IDRow: string,
        readonly DataType: TableInputDataType,
        readonly FullMeritevData: Meritev, //naknadno dodano, ker potrebujemo te podatke za prenos v novo bazo oz. za identifikacijo (primarni kljuc) ob update-anju podatkov
        readonly Column: ColumnDataWrapper, //stolpec v katerem je celica
        public PropertyDefinition: CellPropertyDefinitionCollection,
        initialvalue: CellValues,
        initialEmepData: [EMEPData | undefined, EMEPData | undefined, EMEPData | undefined],              //EMEP zastavice podatka/celice
        veljavnost: boolean = true,
        public ZePreneseniVBazo: boolean = false,
    ) {

        //uredimo EMEP zastavice
        var transformedInitData = initialEmepData.map<EMEPData>(emep => emep != undefined ? emep : EMEPData.Empty);
        this.IntialEmepData = [transformedInitData[0], transformedInitData[1], transformedInitData[2]];
        this.Status = Status.Nespremenjeno;
        this.IntialEmepData.forEach(emepInfo => {
            if (emepInfo.ForceStanje != Stanje.Brez)
                this.Stanje = emepInfo.ForceStanje;
        })
        //shranimo prvotne podatke:
        this.InitialValue = initialvalue;
        this.InitialVeljavnost = veljavnost;
        //in trenutne (so isti, kot prvotni)
        this._value = structuredClone(this.InitialValue);
        this._emepData = this.IntialEmepData;
        this._veljavnost = this.InitialVeljavnost;
        //in spremenjene (so isti, kot prvotni)
        this.ChangedValue = structuredClone(this.InitialValue);
        this.ChangedEmepData = [...this.EmepData];
        this._changedVeljavnost = this.InitialVeljavnost;
        //preverimo, ce katera izmed zastavic forsira neveljavnost podatkov
        this._IsForcedInvalid = this.checkForForceInvalid(this._emepData);
        this._changedIsForcedInvalid = this._IsForcedInvalid;
        //nalozimo Popupdata
        this.PopupData;

    }

    //zapisemo novo zastavico na prvo prosto mesto
    public AddChangedEmepToFirstEmptySpot(newEmep: EMEPData): boolean {
        //ce zastavico ze imamo, je ne bomo spet dodali
        if (this.ChangedEmepData.find(emep => emep.ID == newEmep.ID) != undefined)
            return false;

        //poiscemo index prve nezasedene zastavice
        var index = this.firstEmptyEmepSpotIndex();
        if (index == -1)
            throw Error("Napaka nastavljanja EMEP zastavice. Podatek ze ima zapolnjene vsa mesta!");

        var newEmepArray = this.ChangedEmepData;
        newEmepArray[index] = newEmep;
        this.ChangedEmepData = newEmepArray;
        this._changePopupData = undefined;
        return true;
    }
    //najde prvo prosto mesto za novo emep zastavico
    private firstEmptyEmepSpotIndex(): number {
        if (this.HasFullChangedEmepData)
            return -1;
        else
            return this.ChangedEmepData.findIndex(emep => emep.ID == null);
    }

    //odstranimo emep zastavico, ce jo imamo. Vrne podatek o temu ali se je zastavica odstranila
    public RemoveChangedEmepData(emepToRemove: EMEPData): boolean {
        //varnostno: ce probamo odstraniti empty emep zastavico, je ne moremo
        if (emepToRemove.ID == null)
            return false;

        //ce zastavice nimamo, je ne bomo odstranili
        if (this.ChangedEmepData.find(emep => emep.ID == emepToRemove.ID) == undefined)
            return false;
        //ce pa jo imamo, pa jo odstranimo
        else {
            var newEmep = this.ChangedEmepData.filter(emep => emep.ID != emepToRemove.ID);
            while (newEmep.length < 3) {
                newEmep.push(EMEPData.Empty);
            }
            this.ChangedEmepData = [newEmep[0], newEmep[1], newEmep[2]];
            return true;
        }
    }


    //zapisemo vse nove vrednosti v celico. Vrne true, ce je celica sedaj v stanju spremenjeno
    public CommitChanges(): boolean {
        var s1 = this.CommitChangedEmepData();
        var s2 = this.CommitChangedValue();
        var s3 = this.CommitChangedVeljavnost();
        //console.log({s1: s1, s2: s2, s3:s3});

        //ponastavitev vseh changed vrednosti za ChangedPopupdata
        this.StartChangingCell();
        this._changePopupData = undefined;
        if (s1 == true || s2 == true || s3 == true) {
            this.Status = Status.Spremenjeno;
            return true;
        }
        else {
            this.Status = Status.Nespremenjeno;
            return false;
        }
    }

    //spremenimo vse vrednosti nazaj na prvotne
    public UndoChanges() {
        this.UndoChangedValue();
        this.UndoChangedEmepData();
        this.UndoChangedVeljavnost();
        this.Status = Status.Nespremenjeno;
        this._changePopupData = undefined;
        this._popupData = undefined;
        console.log("undo changes..");
    }
    //pripravimo celico za urejanje
    public StartChangingCell() {
        this.ChangedValue = structuredClone(this.Value);
        this.ChangedEmepData = [...this.EmepData];
        this.ChangedVeljavnost = this.veljavnost;
        this.ChangedIsForcedInvalid = this.IsForcedInvalid;
    }

    //zapišemo novo vrednost celice
    public CommitChangedValue() {
        var spremenjeno = false;
        //preverimo, ce se je kateri izmed property-jev spremeniv
        this.PropertyDefinition.PropertyDefinitions.forEach(prop => {
            if (this.ChangedValue![prop.Property] != this.InitialValue[prop.Property]){
                //console.log({prop: prop,new: this.ChangedValue![prop.Property], old: this.InitialValue[prop.Property]});
                spremenjeno = true;
            }
        });
        this.PropertyDefinition.SpecialPropertyDefinitions.forEach(specProp => {
            if (this.ChangedValue![specProp.Property] != this.InitialValue[specProp.Property]){
                //console.log({specProp: specProp,new: this.ChangedValue![specProp.Property], old: this.InitialValue[specProp.Property]});
                spremenjeno = true;
            }
        });
        this._value = structuredClone(this.ChangedValue!);
        //ker se ob komitanju sprememb spremeni tudi vrednost, ki je v popup-data, jo moramo resetirat
        this._popupData = undefined;
        //console.log("commit values");
        return spremenjeno;
    }

    //ponastavi urejeno vrednost na prvotno
    public UndoChangedValue() {
        this.ChangedValue = structuredClone(this.InitialValue);
    }

    //zapisemo nove vrednosti EMEPZastavic
    public CommitChangedEmepData() {
        var spremenjeno = false;
        this.IntialEmepData.forEach((val) => {
            var v = this.ChangedEmepData.find(newVal => newVal.ID == val.ID);
            if (v == undefined || v == null)
                spremenjeno = true;
        });
        this.ChangedEmepData.forEach((val) => {
            var v = this.IntialEmepData.find(oldVal => oldVal.ID == val.ID);
            if (v == undefined || v == null)
                spremenjeno = true;
        });
        this._emepData = this.ChangedEmepData;
        this.IsForcedInvalid = this.ChangedIsForcedInvalid = this.checkForForceInvalid(this._emepData);
        //posebne EMEP zastavice forsirajo tudi Stanje (poleg statusa)
        this._emepData.forEach(newEmep => {
            if (newEmep.ForceStanje != Stanje.Brez)
                this.Stanje = newEmep.ForceStanje;
        })

        //ker se ob komitanju sprememb spremeni tudi vrednost, ki je v popup-data, jo moramo resetirat
        this._popupData = undefined;

        //ce smo spremenili, gremo v stanje "spremenjeno"
        if (spremenjeno)
            this.Status = Status.Spremenjeno;
        return spremenjeno;
    }
    //ponastavimo urejeno vrednost emep zastavic na prvotno
    public UndoChangedEmepData() {
        this.ChangedEmepData = this.IntialEmepData;
    }

    //ponastavimo veljavnost
    public UndoChangedVeljavnost() {
        this.ChangedVeljavnost = this.InitialVeljavnost;
    }
    //zapisemo novo veljavnost
    public CommitChangedVeljavnost() {
        var spremenjeno = false;
        if (this.ChangedVeljavnost != this.InitialVeljavnost)
            spremenjeno = true;
        this._veljavnost = this.ChangedVeljavnost;
        this._popupData = undefined;
        //console.log("commit veljavnost")
        return spremenjeno;
    }

    //pripravimo podatke za uvoz v novo bazo
    //! trenutno samo za tezke kovine. Verjetno bo treba to logiko spremenit, ko bomo urejali se za druge vrste padavin
    //! Imamo pa v tabeli itak zapisan tip, ki ga posiljamo. Verjetno bi bilo najlazje, da se ta tip poslje v celico in se na podlagi tega tipa potem celica tranformira. Primer za tezke kovine: TableInputDataType.TezkeKovine
    public ToInsertMeritev(): InsertMeritev {
        if (this.DataType == TableInputDataType.TezkeKovine) {
            var tezkeKovineFullMeritevData = this.FullMeritevData as TezkeKovineMeritev;
            return {
                tip_meritve: this.DataType.toString(),
                datum_zajema: tezkeKovineFullMeritevData.termin,
                instalacija: this.IDRow,
                trajanje: this.FullMeritevData.trajanje,
                parameter: Number.parseInt(this.ColumnID),
                emep: this.EmepData.filter(emep => emep.ID != null).map<number>(emep => emep.ID!),
                json_vzorec: {
                    cas_vzorcenja: 0,//TODO
                    oznaka_vzorca: this.IDRow,//TODO
                    lab_stevilka_padavina: 0,//TODO
                },
                json_kontrola: {
                    lod: Number.parseFloat(tezkeKovineFullMeritevData.lod),
                    loq: Number.parseFloat(tezkeKovineFullMeritevData.loq),
                    tkd_premer_lijaka: Number.parseFloat(tezkeKovineFullMeritevData.tkd_premer_lijaka)
                },
                podparametri: {
                    depozicija: Number.parseFloat(this.Value['depozicija'] ?? "0"), //TODO tu bi lahko sicer bral cellPropertyDefinitions in iz tam pobral glavne meritve. Ampak nisem siguren, če je to zares treba, glede na to, da se bojo tej podatki itak generiral za vsako vrsto padavin drugace
                    koncentracija: Number.parseFloat(this.Value['koncentracija'] ?? "0"),
                    koncentrac_suh_use: Number.parseFloat(this.Value['koncentracija_suh_use'] ?? "0"),
                    volumen: tezkeKovineFullMeritevData.volumen,
                    volumen_suh_use: tezkeKovineFullMeritevData.volumen_suh_use,
                    veljavnost: this.veljavnost ? 1 : -1
                }
            } as InsertMeritev;
        }
        throw "Insert za ta tip se ni podprt!";
    }

    public ToUpdateMeritev(): UpdateMeritev {
        return {
            emep: this.EmepData.filter(emep => emep != null && emep.ID != null).map<number>(emep => emep.ID!),
            veljavnost: this.IsForcedInvalid == true ? -1 : 1,
            datum_zajema: this.FullMeritevData.datum_zajema,
            instalacija: this.FullMeritevData.instalacija,
            komentar: this.Value["komentar"] ?? "",
            parameter: this.FullMeritevData.parameter,
            tip_meritve: this.FullMeritevData.tip_meritve,
            trajanje: this.FullMeritevData.trajanje as number,
            //komentar: this.Value["komentar"] ?? ""//TODO matchaj z backendom. ce on vraca "", vracaj "" tudi ti. Ce on vraca undefined, vracaj ot tudi ti
        } as UpdateMeritev;
    }

    //preverimo, ce celica ustreza meritvi
    DerivesFromMeritev(meritev: Meritev): boolean {
        var data = this.FullMeritevData;
        //console.log({ d: data, m: meritev });
        return (
            data.datum_zajema != null &&
            new Date(data.datum_zajema)?.getDate() === new Date(meritev.datum_zajema)?.getDate() &&
            new Date(data.datum_zajema)?.getMonth() === new Date(meritev.datum_zajema)?.getMonth() &&
            new Date(data.datum_zajema)?.getFullYear() === new Date(meritev.datum_zajema)?.getFullYear() &&
            data.instalacija === meritev.instalacija &&
            data.trajanje === meritev.trajanje &&
            data.parameter === meritev.parameter
        )
    }

    Blink(blinkColor: string, duration: number, initialDelay: number = 0) {
        setTimeout(() => {
            clearTimeout(this.blinkTimer);
            this.IsBlinking = true;
            this.BlinkColor = blinkColor;
            //nastavimo timer, da nehamo highlightati cez nekaj sekund
            this.blinkTimer = setTimeout(() => { this.StopBlinking() }, duration);
        }, initialDelay);
    }
    StopBlinking() {
        this.IsBlinking = false;
        this.BlinkColor = "";
    }

    //preverimo, ce katera izmed zastavic forsira neveljavnost podatka
    private checkForForceInvalid(emepData: EMEPData[]) {
        var forcingEmeps = emepData.filter(emep => emep.ForcePower != null)
        //ce nobena zastavica ne forsira nicesar, potem je podatek veljaven: vrnemo false
        if (forcingEmeps == null || forcingEmeps.length == 0)
            return false;

        //drugace pa pogledamo najmocnejsega in vrnemo podatek o tem ali forsira neveljavnost
        return forcingEmeps.reduce((maxElement, currentElement) => {
            return currentElement.ForcePower! > maxElement.ForcePower! ? currentElement : maxElement;
        }).ForceInvalid;

    }
    //odstranimo duplicirane vrednosti emep zastavic
    forceUniqueEmep(value: [EMEPData, EMEPData, EMEPData]): [EMEPData, EMEPData, EMEPData] {
        value.forEach((emepData, index, valueRef) => {
            //ce emepData ni null in pa ce ni brez ID-ja, potem odstranimo vse ostale zastavice, ki imajo isti ID
            if (emepData != null && emepData.ID != null) {
                if (value.filter(emep => emep.ID == emepData.ID).length > 1) {
                    if (valueRef[(index + 1) % 3].ID == emepData.ID)
                        valueRef[(index + 1) % 3] = EMEPData.Empty;
                    if (valueRef[(index + 2) % 3].ID == emepData.ID)
                        valueRef[(index + 2) % 3] = EMEPData.Empty;
                }

            }
        });
        return value;
    }
}
//podatki, ki se prikažejo ob hover-ju
export class PopupData {
    EmepData?: EMEPData[];
    FullInitialPrimaryValue?: string;
    Values?: CellValues;
    IsValid?: boolean;
    PropertyDefinition?: CellPropertyDefinitionCollection;
    get DataOnlyForDisplay(): [string, string][] {
        var cellValues: [string, string][] = [];
        this.PropertyDefinition?.DisplayPropertyDefinitions.forEach(displayDef => {
            var val = this.Values![displayDef.Property];
            if (val != null)
                cellValues.push([displayDef.PropertyDisplayName, val])
        });
        return cellValues;
    }
    get NonNullEmepData() {
        if (this.EmepData == null) {
            this.EmepData = [EMEPData.Empty, EMEPData.Empty, EMEPData.Empty];
        }
        return this.EmepData?.filter(emep => emep?.ID != null);
    }

    get CellValues(): [string, string][] {
        var cellValues: [string, string][] = [];
        this.PropertyDefinition?.PropertyDefinitions.forEach(propertyDef => {
            var val = this.Values![propertyDef.Property];
            cellValues.push([propertyDef.PropertyDisplayName, val ?? "ni podatka"])

        });
        return cellValues;
    }
}

