import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-select-rows-modal.component-modal',
  templateUrl: './select-rows-modal.component.html',
  styleUrls: ['./select-rows-modal.component.css']
})
export class SelectRowsModalComponent implements OnInit {

  public Title! : string;
  public NumberOfRows! : number;
  public NumberOfSelectedRows! : number;
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
  }

  CreateModalResult(onlySelectedRows : boolean):boolean{
    return onlySelectedRows;
  }

}
