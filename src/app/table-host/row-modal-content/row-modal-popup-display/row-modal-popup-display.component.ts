import { Component, Inject, Input, OnInit } from '@angular/core';
import { CellDataWrapper, ColumnDataWrapper, PopupData, SingleRowDataWrapper } from '../../tableClasses';
import { TIPPY_REF, TippyInstance, TippyDirective } from '@ngneat/helipopper';
import { DataService, EMEPData } from 'src/app/services/data.service';
import { DataRepositoryService } from 'src/app/services/data-repository.service';
import { TablePopupDisplayComponent } from '../../popup-display/table-popup-display.component';
import { CommonModule } from '@angular/common'
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CellModalContentComponent } from '../../cell-modal-content/cell-modal-content.component';

///DEPRICATED. WE INSTEAD Table-Popup-Display with different modes
@Component({
  selector: 'app-row-modal-popup-display',
  templateUrl: './row-modal-popup-display.component.html',
  styleUrls: ['./row-modal-popup-display.component.css']
})
export class RowModalPopupDisplayComponent implements OnInit {

  //podatki, ki jih prikazujemo
  DataToDisplay?: PopupData;
  //funkcija, ki se kliče ob kliku na gumb 'Uredi' in parametri za to funkcijo
  RowData!: SingleRowDataWrapper;
  ColumnData!: ColumnDataWrapper;
  TippyInstance : TippyInstance;
  constructor(
    @Inject(TIPPY_REF) tippy: TippyInstance,
    private localStorage: DataRepositoryService,
    public modalService: NgbModal
    ) {
    this.TippyInstance = tippy;
    this.RowData = tippy.data?.RowData;
    this.ColumnData = tippy.data?.ColumnData;
    this.DataToDisplay = tippy.data?.PopupData;

  }

  ngOnInit(): void {
  }



  //metoda se izvede ob kliku na celico
  public EditCellClick() {
    this.TippyInstance.hide();
    const cellModal = this.modalService.open(CellModalContentComponent, { size: 'lg' });
    cellModal.componentInstance.rowData = this.RowData;
    cellModal.componentInstance.columnData = this.ColumnData;
    cellModal.result.then(
      (dismissed) => { console.log("Dismissed modal: " + dismissed) },
      (reason) => {
        console.log(`Dismissed cell ${this.getDismissReason(reason)}`);
      }
    );
  }
  
  //funkcija ponastavi celico na prvotno vrednost
  Ponastavi() {
    this.RowData.getVariableColumnCellData(this.ColumnData).UndoChanges();
  }

  //funkcija se kliče ob zapiranju modala in nam pove na kakšen način je uporabnik modal zaprl
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
