import { KeyValue } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataRepositoryService } from 'src/app/services/data-repository.service';
import { EMEPData } from 'src/app/services/data.service';
import { CellModalContentComponent, CellModalContentMode } from '../cell-modal-content/cell-modal-content.component';
import { TablePopupDisplayComponent, TablePopupDisplayMode } from '../popup-display/table-popup-display.component';
import { AdditionalEmepInfo } from '../table-host.component';
import { SingleRowDataWrapper, CellDataWrapper, RowValues, RowDataWrapper, ColumnDataWrapper, CellValues } from '../tableClasses';
import { RowModalPopupDisplayComponent } from './row-modal-popup-display/row-modal-popup-display.component';



@Component({
  selector: 'app-modal-content',
  templateUrl: './row-modal-content.component.html',
  styleUrls: ['./row-modal-content.component.css']
})
export class RowModalContentComponent implements OnInit {

  @Input()
  public RowData!: RowDataWrapper;
  //način prikazovanja podatkov (dovolimo urejanje, dovolimo samo pregled)
  @Input()
  public mode!: RowModalContentMode;
  public Modes = RowModalContentMode;
  //celice, ki jih highlightamo
  public HighlightedCells: CellDataWrapper[] = [];

  //EMEP zastavica, ki smo jo izbrali za dodajanje, emep zastavica, ki smo jo izbrali za odstranjevanje in njihove posebnosti
  public NewAddEmepData?: EMEPData;
  public NewRemoveEmepData?: EMEPData;
  @Input()
  public emepEndpoint!: string;
  @Input()
  public additionalEmepInfo!: AdditionalEmepInfo[];
  private clearHighlightedCellsTimer?: ReturnType<typeof setTimeout>;
  //vse EMEP zastavice, ki pridejo iz backenda
  allEmepData: EMEPData[] | null = null;

  //opombe vrstice 
  OpombaSKZ!: string | undefined;
  InitialOpombaSKZ!: string | undefined;

  //komponenta za popup-e
  PopupComponent = TablePopupDisplayComponent
  TablePopupDisplayModes = TablePopupDisplayMode;


  constructor(
    public modalService: NgbModal,
    public activeModal: NgbActiveModal,
    private localStorage: DataRepositoryService
  ) { }

  async ngOnInit(): Promise<void> {
    if (this.mode != RowModalContentMode.DisplayOnly) {
      this.RowData.StartChangingCellData();
      this.PridobiOpomboSKZ();
    }
    this.allEmepData = await this.getEmepData();
  }
  public keepOriginalOrder = (a: any, b: any) => a.key

  //pridobimo opombo 
  PridobiOpomboSKZ() {
    //ce imajo vse celice isto opombo, prikazemo to opombo
    if(this.RowData?.PrimaryRow?.GetChangableCells().every(cell => cell.ChangedValue!["komentar"] == this.RowData?.PrimaryRow?.GetChangableCells()[0]?.ChangedValue!["komentar"]))
    {
      this.InitialOpombaSKZ = this.RowData?.PrimaryRow.GetChangableCells()[0].ChangedValue!["komentar"] ?? "";
    }else{
      this.InitialOpombaSKZ = ""; 
    }
    this.OpombaSKZ = this.InitialOpombaSKZ;
  }
  PonastaviOpomboSKZ() {
    console.log({ init: this.InitialOpombaSKZ, new: this.OpombaSKZ });
    this.OpombaSKZ = this.InitialOpombaSKZ;
  }

  //pridobimo vse podatke neke celice
  public GetCellDisplayValue(col: ColumnDataWrapper, property: keyof CellValues) {
    var val = this.RowData.PrimaryRow.getVariableColumnCellData(col).ChangedValue![property]
    return val?.substring(0, 5);
  }

  public GetCellInitialValue(col: ColumnDataWrapper, property: keyof CellValues) {
    return this.RowData.PrimaryRow.getVariableColumnCellData(col).Value![property];
  }
  public GetCellInitialDisplayValue(col: ColumnDataWrapper, property: keyof CellValues) {
    var val = this.RowData.PrimaryRow.getVariableColumnCellData(col).Value![property]
    if (val != null && val.length > 5)
      return val.substring(0, 5);
    else return val;
  }
  //pridobi definicije od vseh vrednosti v celici
  public GetAllCellDataProperties() {
    return this.RowData.CellPropertyDefinitions.PropertyDefinitions;
  }
  //pridobimo PopupData celice
  public GetCellChangedPopupData(col: ColumnDataWrapper) {
    return this.RowData.PrimaryRow.getVariableColumnCellData(col).ChangePopupData
  }

  //preverimo ali celica ima vsaj eno Emep zastavico.
  public CellHasEmepData(col: ColumnDataWrapper): boolean {
    return this.RowData.PrimaryRow.getVariableColumnCellData(col).ChangePopupData.NonNullEmepData.length != 0;
  }
  //preverimo ali celica ima vsaj en podatek samo za prikaz (komentar)
  public CellHasDisplayProperties(col: ColumnDataWrapper) {
    return this.RowData.PrimaryRow.getVariableColumnCellData(col).PopupData.DataOnlyForDisplay.length != 0;
  }
  //pridobimo glavni popupdata (v tem mamo tudi komentarje)
  public GetCellPopupData(col: ColumnDataWrapper) {
    return this.RowData.PrimaryRow.getVariableColumnCellData(col).PopupData
  }

  //metoda se izvede ob kliku na celico
  editCellClick(rowData: SingleRowDataWrapper, cellData: ColumnDataWrapper) {
    const cellModal = this.modalService.open(CellModalContentComponent, { size: 'lg' });
    cellModal.componentInstance.rowData = rowData;
    cellModal.componentInstance.columnData = cellData;
    cellModal.result.then(
      (dismissed) => { console.log("Dismissed modal: " + dismissed) },
      (reason) => {
        console.log(`Dismissed cell ${this.getDismissReason(reason)}`);
      }
    );
  }
  //funkcija se kliče ob zapiranju modala in nam pove na kakšen način je uporabnik modal zaprl
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  //#region HIGHLIGHTANJE
  //highlightamo celice, ki jim zastavice forsirajo neveljavnost
  private highlightForcedInvalidCells() {
    var highlightedRows: CellDataWrapper[] = [];
    highlightedRows = [...this.RowData.PrimaryRow.GetChangableCells().filter(cell => cell.ChangedIsForcedInvalid)]
    this.RowData.SecondaryRows?.forEach(
      row =>
        highlightedRows = highlightedRows.concat(row.GetChangableCells().filter(cell => cell.ChangedIsForcedInvalid)))
    this.HighlightedCells = highlightedRows;
  }
  private highlightFullEMEPDataCells() {
    var highlightedRows: CellDataWrapper[] = [];
    highlightedRows = [...this.RowData.PrimaryRow.GetChangableCells().filter(cell => cell.HasFullChangedEmepData)]
    this.RowData.SecondaryRows?.forEach(
      row =>
        highlightedRows = highlightedRows.concat(row.GetChangableCells().filter(cell => cell.HasFullChangedEmepData)))
    this.HighlightedCells = highlightedRows;
  }

  //preverimo, ali je celica highlightana
  public IsHighlighted(cell: CellDataWrapper) {
    return this.HighlightedCells.find(c => c == cell) != undefined;
  }
  //prenehamo highlightat
  private stopHighlighting() {
    this.HighlightedCells = [];
  }
  //highlightamo celice, ki so trenutno izbrane za highlightanje, za dolocen cas
  private async displayHighlightedCells(timer: number) {
    if (this.HighlightedCells == null || this.HighlightedCells.length == 0)
      return;

    if (this.clearHighlightedCellsTimer != null)
      clearTimeout(this.clearHighlightedCellsTimer);
    //nastavimo timer, da nehamo highlightati cez nekaj sekund
    this.clearHighlightedCellsTimer = setTimeout(() => { this.stopHighlighting() }, timer);
  }
  //pridobimo stil (okvirja  neke celice)
  public GetCellStyle(cell: CellDataWrapper) {
    if (this.mode == RowModalContentMode.Edit || this.mode == RowModalContentMode.EditOnlyEMEPAndValidity)
      return cell.ChangedVeljavnost == true ? 'cell-bg-veljaven' : 'cell-bg-neveljaven';
    else
      return cell.veljavnost == true ? 'cell-bg-veljaven' : 'cell-bg-neveljaven';
  }
  //prikazi celice, ki jim zastavice forsirajo neveljavnost
  public async DisplayHighlightedForcedInvalidCells() {
    if (this.CanSetAllVeljavnostToTrue())
      return;
    //najdemo celice za highlight
    this.highlightForcedInvalidCells();
    //sprozimo highlightanje
    this.displayHighlightedCells(3000);
  }

  //prikazi celice, ki imajo ze 3 zastavice
  public async DisplayHighlightedFullEMEPDataCells() {
    if (this.CanSetAllCellEmepData())
      return;
    //najdemo celice za highlight
    this.highlightFullEMEPDataCells();
    //sprozimo highlightanje
    this.displayHighlightedCells(2500);
  }

  //#endregion

  //#region SPREMINJANJE VELJAVNOSTI
  //ali lahko spremenimo vse podatke na veljavne
  CanSetAllVeljavnostToTrue() {
    var forcedCells: CellDataWrapper[] = [];
    forcedCells = [...this.RowData.PrimaryRow.GetChangableCells().filter(cell => cell.ChangedIsForcedInvalid)]
    if (forcedCells.length != 0)
      return false;
    this.RowData.SecondaryRows?.forEach(
      row =>
        forcedCells = forcedCells.concat(row.GetChangableCells().filter(cell => cell.ChangedIsForcedInvalid)))
    if (forcedCells.length != 0)
      return false;
    else
      return true;
  }

  //nastavimo vse vrednosti vrstice kot veljavne
  SetAllCellValidity(newValue: boolean) {
    //celice, ki smo jim spremenili vrednost, bomo highlightali
    this.HighlightedCells = [];

    //primarna vrsta
    this.RowData.PrimaryRow.GetChangableCells().forEach(cell => {
      if (cell.ChangedVeljavnost != newValue)
        this.HighlightedCells.push(cell);
      cell.ChangedVeljavnost = newValue
    });
    //sekundarne vrste
    if (this.RowData.SecondaryRows != null && this.RowData.SecondaryRows.length != 0)
      this.RowData.SecondaryRows.forEach(row => row.GetChangableCells().forEach(cell => {
        if (cell.ChangedVeljavnost != newValue)
          this.HighlightedCells.push(cell);
        cell.ChangedVeljavnost = newValue
      }));

    //highlightamo spremenjene celice
    this.displayHighlightedCells(800);
  }
  //#endregion

  //#region SPREMINJANJE EMEP ZASTAVIC
  //pridobimo vse EMEP zastavice
  //pridobimo emep zastavice
  private async getEmepData(includeEmpty: boolean = false): Promise<EMEPData[]> {
    var data = await this.localStorage.GetEmepData(this.emepEndpoint, this.additionalEmepInfo, includeEmpty);
    return data;
  }
  //Izberemo novo EMEP zastavico za dodajanje
  public SetAddEmepData(newEmep: EMEPData) {
    if (newEmep == null)
      return;
    this.NewAddEmepData = newEmep;
  }

  public SetRemoveEmepData(removeEmep: EMEPData) {
    if (removeEmep == null)
      return;
    this.NewRemoveEmepData = removeEmep;
  }


  //ali lahko vsem celicam nastavimo EMEP zastavico (celice, ki ze imajo 3 zastavice, ne morajo dobiti nove)
  CanSetAllCellEmepData() {
    var fullCells: CellDataWrapper[] = [];
    fullCells = [...this.RowData.PrimaryRow.GetChangableCells().filter(cell => cell.HasFullChangedEmepData)]
    if (fullCells.length != 0)
      return false;
    this.RowData.SecondaryRows?.forEach(
      row =>
        fullCells = fullCells.concat(row.GetChangableCells().filter(cell => cell.HasFullChangedEmepData)))
    if (fullCells.length != 0)
      return false;
    else
      return true;
  }

  //nastavimo EMEP zastavico vsem celicam v vrstici
  SetEmepDataToAll() {
    this.HighlightedCells = [];
    //ce ni izbrana nobena zastavic, vrnemo error
    if (this.NewAddEmepData == null) {
      alert("Izbrati morate EMEP zastavico");
      return;
    }
    //primarna vrsta
    this.RowData.PrimaryRow.GetChangableCells().forEach(cell => {
      if (cell.AddChangedEmepToFirstEmptySpot(this.NewAddEmepData!))
        this.HighlightedCells.push(cell);
    });
    //sekundarne vrste
    //!depricated
    if (this.RowData.SecondaryRows != null && this.RowData.SecondaryRows.length != 0)
      this.RowData.SecondaryRows.forEach(row => row.GetChangableCells().forEach(cell => {
        if (cell.AddChangedEmepToFirstEmptySpot(this.NewAddEmepData!))
          this.HighlightedCells.push(cell);
      }));

    //highlightamo spremenjene celice
    this.displayHighlightedCells(1200);
  }

  //odstranimo EMEP zastavico vsem celicam v vrstici
  UnsetEmepDataToAll() {
    this.HighlightedCells = [];
    //ce ni izbrana nobena zastavic, vrnemo error
    if (this.NewRemoveEmepData == null) {
      alert("Izbrati morate EMEP zastavico za odstranjevanje");
      return;
    }
    //primarna vrsta
    this.RowData.PrimaryRow.GetChangableCells().forEach(cell => {
      if (cell.RemoveChangedEmepData(this.NewRemoveEmepData!))
        this.HighlightedCells.push(cell);
    });
    //sekundarne vrste
    if (this.RowData.SecondaryRows != null && this.RowData.SecondaryRows.length != 0)
      this.RowData.SecondaryRows.forEach(row => row.GetChangableCells().forEach(cell => {
        if (cell.RemoveChangedEmepData(this.NewRemoveEmepData!))
          this.HighlightedCells.push(cell);
      }));

    //highlightamo spremenjene celice
    this.displayHighlightedCells(1200);
  }
  //#endregion


  //da se property-ji pokazejo v pravilnem vrstnem redu
  trackByFn(index: any, item: any) {
    return index;
  }

  //zapri modal in shrani rezultate
  CloseSave() {
    if (this.mode == RowModalContentMode.DisplayOnly)
      return;



    if (this.OpombaUpdatedByUser()) {
      //ker posebej urejamo opombe, jih moramo prepisat na vse celice
      this.RowData.PrimaryRow.GetChangableCells().forEach(cell => {
        cell.ChangedValue!["komentar"] = this.OpombaSKZ;
      });
    }
    //primarna vrsta
    this.RowData.PrimaryRow.GetChangableCells().forEach(cell => cell.CommitChanges());
    //sekundarne vrste (depricated)
    if (this.RowData.SecondaryRows != null && this.RowData.SecondaryRows.length != 0)
      this.RowData.SecondaryRows.forEach(row => row.GetChangableCells().forEach(cell => cell.CommitChanges()));
    this.activeModal.close();
  }

  OpombaUpdatedByUser() {
    return this.InitialOpombaSKZ != this.OpombaSKZ;
  }

  closeCancel() {
    this.activeModal.dismiss();
  }
  stringify(value: any) {
    return JSON.stringify(value);
  }


  //metoda se izvede ob kliku na celico
  public EditCellClick(col: ColumnDataWrapper) {
    const cellModal = this.modalService.open(CellModalContentComponent, { size: 'lg' });
    cellModal.componentInstance.rowData = this.RowData;
    cellModal.componentInstance.columnData = col;
    cellModal.componentInstance.mode = CellModalContentMode.FromRowMode;
    cellModal.componentInstance.emepEndpoint = this.emepEndpoint;
    cellModal.componentInstance.additionalEmepInfo = this.additionalEmepInfo;
    cellModal.result.then(
      (dismissed) => { console.log("Dismissed modal: " + dismissed) },
      (reason) => {
        console.log(`Dismissed cell ${this.getDismissReason(reason)}`);
      }
    );
  }
}
export enum RowModalContentMode {
  Edit,
  EditOnlyEMEPAndValidity,
  DisplayOnly
}


