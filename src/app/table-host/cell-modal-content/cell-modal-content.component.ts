import { Component, Input, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DataRepositoryService } from 'src/app/services/data-repository.service';
import { EMEPData } from 'src/app/services/data.service';
import { TablePopupDisplayComponent, TablePopupDisplayMode } from '../popup-display/table-popup-display.component';
import { AdditionalEmepInfo } from '../table-host.component';
import { SingleRowDataWrapper, CellDataWrapper, ColumnDataWrapper, RowValues, RowDataWrapper, CellValues } from '../tableClasses';

//pove v kakšnem mode-u delamo. 
//včasih spreminjamo samo vrednosti celice in jih ne commitamo,
//včasih (v standalone nacinu) pa urejamo in commitamo celice
export enum CellModalContentMode {
  StandaloneEdit,
  FromRowMode,
  DisplayOnly,
  EditOnlyEMEP
}


@Component({
  selector: 'app-cell-modal-content',
  templateUrl: './cell-modal-content.component.html',
  styleUrls: ['./cell-modal-content.component.css']
})
export class CellModalContentComponent implements OnInit {

  @Input()
  public rowData!: RowDataWrapper;
  @Input()
  public columnData?: ColumnDataWrapper;
  @Input()
  public emepEndpoint!: string;
  @Input()
  public additionalEmepInfo!: AdditionalEmepInfo[];
  @Input()
  public mode!: CellModalContentMode;
  public CellData!: CellDataWrapper;
  public CellValues!: CellValues;
  public allEmepData: EMEPData[] | null = null;
  public modes = CellModalContentMode;
  public PopupComponent = TablePopupDisplayComponent;
  public TablePopupDisplayModes = TablePopupDisplayMode;

  //opombe vrstice 
  OpombaSKZ!: string | undefined;
  InitialOpombaSKZ!: string | undefined;


  constructor(
    public activeModal: NgbActiveModal,
    private localStorage: DataRepositoryService) { }

  //celico spravimo v stanje urejanja
  async ngOnInit(): Promise<void> {
    this.CellData = this.rowData.PrimaryRow.getVariableColumnCellData(this.columnData!);
    this.initMode();
    
    this.CellValues = this.CellData.ChangedValue!;
    console.log(this.CellValues);
    this.allEmepData = await this.getEmepData();
  }

  private initMode() {
    switch (this.mode) {
      case CellModalContentMode.StandaloneEdit:
        //v standalone nacinu moramo celico spraviti v stanje za urejanje
        this.CellData?.StartChangingCell();
        this.PridobiOpomboSKZ();
        return;
      case CellModalContentMode.FromRowMode:
        //v tem nacinu urejamo posamezno celico, ko uporabnik ureja posamezno vrstico. 
        //celice zato pridejo ze v nacinu za urejanje
        return;
      case CellModalContentMode.DisplayOnly:
        //v tem nacinu uporabnik ne mora spreminjati celice
        return;
      case CellModalContentMode.EditOnlyEMEP:
        //v tem nacinu uporabnik lahko spreminja veljavnosti in zastavice, ne mora pa spreminjati vrednosti
        this.PridobiOpomboSKZ();
        return;
      default:
        throw Error("Način urejanja celice je neprepoznan ali nespecificiran! " + this.mode);
    }

  }
  //pridobimo vse vrednosti celice
  public GetCellProperties() {
    return this.CellData.PropertyDefinition.PropertyDefinitions;
  }

  public DisplayButtons(): boolean {
    return this.mode != CellModalContentMode.DisplayOnly;
  }
  public DisplayEditValues(): boolean {
    return this.DisplayButtons() && this.mode != CellModalContentMode.EditOnlyEMEP && this.mode != CellModalContentMode.FromRowMode;
  }
  //revertamo vrednost celice na prvotno vrednost
  Undo() {
    this.CellData.UndoChanges();
    this.CellValues = this.CellData.ChangedValue!;
    //PonastaviOpomboSKZ
  }
  //ali je celici forsirana neveljavnost preko zastavice
  IsForcedInvalid() {
    return this.CellData.ChangedIsForcedInvalid;
  }
  //pridobimo podatek o tem ali ima celica kaj podatkov, ki so samo za prikaz
  public CellHasDataOnlyForDisplay() {
    return this.CellData.PopupData.DataOnlyForDisplay.length != 0;
  }
  //pridobimo PopupData za prikaz dodatnih podatkov
  public GetCellPopupData() {
    return this.CellData.PopupData;
  }

  //pridobimo emep zastavice
  private async getEmepData(): Promise<EMEPData[]> {
    var data = await this.localStorage.GetEmepData(this.emepEndpoint, this.additionalEmepInfo);
    //console.log("emep data");
    //console.log(data);

    return data;
  }

  //spremenimo zastavico podatka
  //index - zap. st. zastavice
  //newEmepData - nova zastavico
  //vsi podatki imajo iste zastavice
  public SetEmepData(index: number, newEmepData: EMEPData) {
    var newEmep: [EMEPData, EMEPData, EMEPData] = [...this.CellData.ChangedEmepData];
    newEmep[index] = newEmepData;
    this.CellData.ChangedEmepData = newEmep;
  }
  //spremenimo veljavnost celice
  public ChangeVeljavnost(novaVeljavnost: boolean) {
    this.CellData.ChangedVeljavnost = novaVeljavnost;

  }

  //shranimo nove podatke celice in zapremo modal
  public SaveDataAndClose() {
    if (this.mode == CellModalContentMode.StandaloneEdit)
      this.rowData.PrimaryRow.UpdateCellAllData(this.CellData);
    else if (this.mode == CellModalContentMode.EditOnlyEMEP){
      this.CellData.ChangedValue!["komentar"] = this.OpombaSKZ; 
      this.rowData.PrimaryRow.UpdateCellEMEPData(this.CellData);
      this.rowData.PrimaryRow.UpdateCellAllData(this.CellData);
    }

    this.activeModal.close();
  }

  ///spreminjamo opombe
  //pridobimo opombo (kar iz prve celice, ker morajo imeti vse celice v vrsti enake opombe)
  PridobiOpomboSKZ() {
    this.InitialOpombaSKZ = this.CellData.Value!["komentar"];
    this.OpombaSKZ = this.InitialOpombaSKZ;
  }
  PonastaviOpomboSKZ() {
    this.OpombaSKZ = this.InitialOpombaSKZ;
  }
}

