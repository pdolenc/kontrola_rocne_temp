import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[TableDirective]'
})
export class TableDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
